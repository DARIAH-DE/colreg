package eu.dariah.de.colreg.config;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import eu.dariah.de.colreg.config.nested.IngestSource;
import lombok.Data;

@Data
@Configuration
@ConfigurationProperties(prefix = "ingest")
public class IngestConfig {
	  private boolean useIpFilter;
	  private boolean useApiKey;
	  private List<IngestSource> sources;
}
