package eu.dariah.de.colreg.config.nested;

import lombok.Data;

@Data
public class PathsConfigProperties {
	private String main;
	private String data;
	private String images;
	private String backups;
	private String testdata;
}