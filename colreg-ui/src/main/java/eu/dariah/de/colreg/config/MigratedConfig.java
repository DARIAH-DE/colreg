package eu.dariah.de.colreg.config;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.PostConstruct;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;

import de.unibamberg.minf.core.web.service.ImageService;
import de.unibamberg.minf.core.web.service.ImageServiceImpl;
import de.unibamberg.minf.core.web.service.ImageServiceImpl.SizeBoundsType;
import de.unibamberg.minf.processing.git.adapter.GitRepositoryAdapterImpl;
import de.unibamberg.minf.processing.git.service.GitDataSourceService;
import de.unibamberg.minf.processing.git.service.GitDataSourceServiceImpl;
import eu.dariah.de.colreg.config.nested.ImagesConfigProperties;
import eu.dariah.de.colreg.config.nested.PathsConfigProperties;
import eu.dariah.de.colreg.config.nested.ThumbnailsConfigProperties;
import eu.dariah.de.colreg.controller.automation.AccessTestsCleanupService;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
@Configuration
@ConfigurationProperties
public class MigratedConfig {
	
	private PathsConfigProperties paths; 
	private ImagesConfigProperties images;
	
	
	@PostConstruct
    public void completeConfiguration() {
		if (paths.getData()==null) {
			paths.setData(Paths.get(paths.getMain(), "data").toString());
		}
		if (paths.getImages()==null) {
			paths.setImages(Paths.get(paths.getMain(), "images").toString());
		}
		if (paths.getBackups()==null) {
			paths.setBackups(Paths.get(paths.getMain(), "backups").toString());
		}
		if (paths.getTestdata()==null) {
			paths.setTestdata(Paths.get(paths.getMain(), "testdata").toString());
		}
		
		if (images==null) {
			images = new ImagesConfigProperties();
			images.setThumbnails(new ThumbnailsConfigProperties());
		}
    }

	/*@Bean
    public Jaxb2Marshaller jaxb2Marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setClassesToBeBound(new Class[]{
           //all the classes the context needs to know about
        		eu.dariah.de.colreg.model.api.ActionObject.class,
        		eu.dariah.de.colreg.model.Agent.class,
        		eu.dariah.de.colreg.model.Collection.class,
        		eu.dariah.de.colreg.model.api.repository.RepositoryDraftContainer.class,
        		eu.dariah.de.colreg.model.api.repository.RepositoryResponse.class,
        		eu.dariah.de.colreg.model.api.oaipmh.OaiPmhResponseContainer.class
        });
        marshaller.setMarshallerProperties(new HashMap<>() {{
          put(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT, true);
        }});

        return marshaller;
    }
	
	@Bean
	public XMLConverter xmlConverter() {
		XMLConverter xmlConverter = new XMLConverter();
		xmlConverter.setMarshaller(jaxb2Marshaller());
		xmlConverter.setUnmarshaller(jaxb2Marshaller());
		return xmlConverter;
	}*/
	
	/*@Bean
	public RequestMappingHandlerAdapter requestMappingHandlerAdapter() {
		RequestMappingHandlerAdapter requestMappingHandlerAdapter = new RequestMappingHandlerAdapter();
		requestMappingHandlerAdapter.setMessageConverters(new ArrayList<>());
		requestMappingHandlerAdapter.getMessageConverters().add(jsonConverter());
		
		return requestMappingHandlerAdapter;
	}*/
	
	/*@Bean
	public org.springframework.http.converter.json.MappingJackson2HttpMessageConverter jsonConverter() {
		return new MappingJackson2HttpMessageConverter(objectMapper());
	}*/
	
	@Bean
	public ImageService imageService() {
		ImageServiceImpl imageService = new ImageServiceImpl();
		imageService.setImagePath(paths.getImages());
		imageService.setImagesHeight(images.getHeight());
		imageService.setImagesWidth(images.getWidth());
		imageService.setImagesSizeType(SizeBoundsType.MAX_LONGEST_SIDE);
		imageService.setThumbnailsHeight(images.getThumbnails().getHeight());
		imageService.setThumbnailsWidth(images.getThumbnails().getWidth());
		imageService.setThumbnailsSizeType(SizeBoundsType.MAX_SHORTEST_SIDE);
		return imageService;
	}
	
	@Bean
	public GitDataSourceService gitDataSourceService() {
		GitDataSourceServiceImpl gitDataSourceService = new GitDataSourceServiceImpl();
		gitDataSourceService.setAdapter(new GitRepositoryAdapterImpl());
		gitDataSourceService.setBaseDirectory(paths.getTestdata());
		return gitDataSourceService;
	}
	
	@Bean
	public AccessTestsCleanupService accessTestsCleanupService() {
		AccessTestsCleanupService accessTestsCleanupService = new AccessTestsCleanupService();
		accessTestsCleanupService.setRun(true);
		accessTestsCleanupService.setBaseDirectory(paths.getTestdata());
		return accessTestsCleanupService;
	}
	
	@Bean
	public ObjectMapper objectMapper() {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.registerModule(new JodaModule());
		
		return objectMapper;
	}
}