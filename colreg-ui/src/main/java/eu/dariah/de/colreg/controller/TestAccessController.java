package eu.dariah.de.colreg.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;

import de.unibamberg.minf.core.web.localization.MessageSource;
import de.unibamberg.minf.core.web.pojo.ModelActionPojo;
import de.unibamberg.minf.processing.git.model.GitDataSource;
import de.unibamberg.minf.processing.git.service.GitDataSourceService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


import eu.dariah.de.colreg.pojo.api.TestAccessSourcePojo;

@Controller
@RequestMapping("/testAccess")
public class TestAccessController {
	@Autowired private GitDataSourceService gitDataSourceService;
	@Autowired private MessageSource messageSource;
	
	@RequestMapping(value="/form", method=RequestMethod.POST)
	public String getForm(@RequestBody TestAccessSourcePojo accessSource, HttpServletRequest request, Model model, Locale locale) {
		return "testAccess/form";
	}	
	
	@RequestMapping(value="/tests/accessible", method=RequestMethod.POST)
	public @ResponseBody ModelActionPojo testAccessible(@RequestBody TestAccessSourcePojo accessSource, Locale locale) {
		ModelActionPojo result = new ModelActionPojo();
		
		if (accessSource.getUri()==null || accessSource.getUri().trim().isEmpty()) {
			result.addObjectError(messageSource.getMessage("~eu.dariah.de.colreg.view.access.test.errors.no_url", null, locale));
			return result;
		}
		
		GitDataSource dataSource = new GitDataSource();
		dataSource.setRepositoryUrl(accessSource.getUri());
		
		if (!gitDataSourceService.checkRepositoryExists(dataSource)) {
			result.addObjectError(messageSource.getMessage("~eu.dariah.de.colreg.view.access.test.errors.not_accessible", null, locale));
			return result;
		}
		
		result.setSuccess(true);
		return result;
	}

	@RequestMapping(value="/tests/clone", method=RequestMethod.POST)
	public @ResponseBody ModelActionPojo testClone(@RequestBody TestAccessSourcePojo accessSource, Locale locale) {
		ModelActionPojo result = new ModelActionPojo();
		
		GitDataSource dataSource = new GitDataSource();
		dataSource.setRepositoryUrl(accessSource.getUri());
		dataSource.setBranchOrTagName(accessSource.getSet()==null || accessSource.getSet().trim().isEmpty() ? null : accessSource.getSet());
		dataSource.setWorkingDirectoryName(accessSource.getUuid());

		// Folder exists, but is not recognizable as git working directory or not matching provided URI
		if (gitDataSourceService.checkDirectoryExists(dataSource) && !gitDataSourceService.checkRepositoryExistsLocal(dataSource)) {
			if (!gitDataSourceService.deleteRepository(dataSource)) {
				result.addObjectError(messageSource.getMessage("~eu.dariah.de.colreg.view.access.test.errors.failed_to_delete", null, locale));
			}
		}
		
		if (gitDataSourceService.checkRepositoryExistsLocal(dataSource)) {
			if (!gitDataSourceService.pullRepository(dataSource)) {
				result.addObjectError(messageSource.getMessage("~eu.dariah.de.colreg.view.access.test.errors.failed_to_pull", null, locale));
			}
		} else {
			dataSource.setBranchOrTagName(null);
			if (!gitDataSourceService.cloneRepository(dataSource)) {
				result.addObjectError(messageSource.getMessage("~eu.dariah.de.colreg.view.access.test.errors.failed_to_clone", null, locale));
			}
			dataSource.setBranchOrTagName(accessSource.getSet()==null || accessSource.getSet().trim().isEmpty() ? null : accessSource.getSet());
		}
		
		if (dataSource.getBranchOrTagName()!=null) {
			if (!gitDataSourceService.checkoutBranch(dataSource)) {
				List<String> branches = gitDataSourceService.getRemoteBranchesAndTags(dataSource);
				StringBuilder strBranches = new StringBuilder();
				for (int i=0; i<branches.size(); i++) {
					strBranches.append(branches.get(i));
					if (i<branches.size()-1) {
						strBranches.append(", ");
					}
				}
				result.addObjectError(messageSource.getMessage("~eu.dariah.de.colreg.view.access.test.errors.failed_to_checkout", new Object[] { strBranches.toString() }, locale));
			}
		}
		
		if (!result.hasErrors()) {
			result.setSuccess(true);
		}
		return result;
	}
	
	
	@RequestMapping(value="/tests/patterns", method=RequestMethod.POST)
	public @ResponseBody ModelActionPojo testPatterns(@RequestBody TestAccessSourcePojo accessSource, Locale locale) {
		ModelActionPojo result = new ModelActionPojo();
		
		GitDataSource dataSource = new GitDataSource();
		dataSource.setRepositoryUrl(accessSource.getUri());
		dataSource.setBranchOrTagName(accessSource.getSet()==null || accessSource.getSet().trim().isEmpty() ? null : accessSource.getSet());
		dataSource.setWorkingDirectoryName(accessSource.getUuid());
		dataSource.setResourceFilePatterns(new ArrayList<>());
		
		if (accessSource.getPatterns()!=null) {
			for (String pattern : accessSource.getPatterns()) {
				if (pattern!=null && !pattern.trim().isEmpty() && !dataSource.getResourceFilePatterns().contains(pattern.trim())) {
					dataSource.getResourceFilePatterns().add(pattern.trim());
				}
			}
		}
				
		if (dataSource.getResourceFilePatterns().isEmpty()) {
			result.addObjectError(messageSource.getMessage("~eu.dariah.de.colreg.view.access.test.errors.no_pattern_provided", null, locale));
		} else {
			List<String> validPatterns = new ArrayList<>();
			List<String> invalidPatterns = new ArrayList<>();
			gitDataSourceService.validatePatternSyntax(dataSource, validPatterns, invalidPatterns);
			if (!invalidPatterns.isEmpty()) {
				for (String invalidPattern : invalidPatterns) {
					result.addObjectWarning(messageSource.getMessage("~eu.dariah.de.colreg.view.access.test.errors.invalid_pattern_i", new Object[] { invalidPattern }, locale));
				}
			}
			
			result.setPojo(gitDataSourceService.getRelativePaths(dataSource));
			if (validPatterns.size()>0) {
				result.setSuccess(true);
			}
			return result;
		}
		
	
		return result;
	}
}
