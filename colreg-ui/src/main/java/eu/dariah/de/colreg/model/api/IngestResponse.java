package eu.dariah.de.colreg.model.api;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="response")
public class IngestResponse {
	private String status;
	private String collectionState;
	private String collectionId;
	private String url;
	private String xml;
	private String json;
	private String error;
	
	@XmlElement(name="status")
	public String getStatus() { return status; }
	public void setStatus(String status) { this.status = status; }
	
	@XmlElement(name="collectionState")
	public String getCollectionState() { return collectionState; }
	public void setCollectionState(String collectionState) { this.collectionState = collectionState; }
	
	@XmlElement(name="collectionId")
	public String getCollectionId() { return collectionId; }
	public void setCollectionId(String collectionId) { this.collectionId = collectionId; }
	
	@XmlElement(name="url")
	public String getUrl() { return url; }
	public void setUrl(String url) { this.url = url; }
	
	@XmlElement(name="xml")
	public String getXml() { return xml; }
	public void setXml(String xml) { this.xml = xml; }
	
	@XmlElement(name="json")
	public String getJson() { return json; }
	public void setJson(String json) { this.json = json; }
	
	@XmlElement(name="error")
	public String getError() { return error; }
	public void setError(String error) { this.error = error; }	
}