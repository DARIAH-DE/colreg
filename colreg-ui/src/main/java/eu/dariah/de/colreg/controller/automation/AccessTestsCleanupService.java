package eu.dariah.de.colreg.controller.automation;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;

import org.joda.time.DateTime;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class AccessTestsCleanupService implements InitializingBean, DisposableBean {
	
	protected boolean run = false;
	private int interval = 86400;	 // Default: 1 day
	private int timeout = 180;		 // Default: 30 minutes
	
	private String baseDirectory;

	private ExecutorService syncExecutor;
	private Timer syncTimer = null;
	
	private TimerTask timeoutTimerTask;
	private DateTime lastSyncTimestamp = null;
	private DateTime nextSyncTimestamp = null;
	private boolean inProgress = false;
	
	private ReentrantLock setupLock = new ReentrantLock();

	
	@Override
	public void afterPropertiesSet() throws Exception {
		if (baseDirectory==null) {
			throw new BeanInitializationException("Base working directory cannot be NULL");
		}

		File workingDir = new File(baseDirectory);
		if (workingDir.exists()) {
			if(!workingDir.canWrite()) {
				throw new BeanInitializationException("Base working directory is not writable");
			}
		}	
		if (this.isRun()) {
			this.syncTimer = new Timer();
			this.setupSyncTimer();
		}
		log.info("Initialized AccessTestsCleanupService with base working directory [{}]", workingDir.getAbsolutePath());
	}
	
	private void setupSyncTimer() {
		TimerTask syncTimerTask = new TimerTask() { 
			public void run() { 
				try {					
					runAsync();
					nextSyncTimestamp = DateTime.now().plusSeconds(getInterval());
				} catch (Exception e) {
					log.error("Failed to execute sync task", e);
				}
			}
		};
		nextSyncTimestamp = DateTime.now().plusSeconds(getInterval());
		syncTimer.scheduleAtFixedRate(syncTimerTask, this.getInterval()*1000, this.getInterval()*1000);
		log.info(String.format("Scheduled synchronization every %s seconds", this.getInterval()));
	}
	
	protected boolean runAsync() {
		try {
			if (syncExecutor == null || syncExecutor.isShutdown() || syncExecutor.isTerminated()) {
				syncExecutor = Executors.newSingleThreadExecutor();
			}
			
			// For interrupting the thread if necessary
			Timer timeoutTimer = new Timer();
			timeoutTimerTask = new TimeoutTimerTask(syncExecutor.submit(new AccessTestsCleanupCallable(baseDirectory)), syncExecutor);
			timeoutTimer.schedule(timeoutTimerTask, this.timeout * 1000);		
			
			return true;
		} catch (Exception e) {
			log.error("An error occurred while executing synchronization", e);
			return false;
		}
	}
	
	
	@Override
	public void destroy() throws Exception {
		if (syncExecutor==null) {
			return;
		}
		try {
			syncExecutor.shutdown();
		    // Wait until all threads are finished
		    while (!syncExecutor.isTerminated()) {}
		} catch (final Exception e) {
			log.error("Error closing sync executor", e);
		}
	}
}
