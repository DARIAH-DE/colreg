package eu.dariah.de.colreg.controller.automation;

import java.io.File;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.concurrent.Callable;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.AgeFileFilter;
import org.apache.commons.io.filefilter.FileFilterUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AccessTestsCleanupCallable implements Callable<Boolean> {

	private String cleanupDirectory;
	
	public AccessTestsCleanupCallable(String cleanupDirectory) {
		this.cleanupDirectory = cleanupDirectory;
	}
	
	@Override
	public Boolean call() throws Exception {
		try {
			Date threshold = Date.from(LocalDate.now().minusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant());
			AgeFileFilter filter = new AgeFileFilter(threshold);
			   
			File path = new File(this.cleanupDirectory);
			File[] outagedTestDirs = FileFilterUtils.filter(filter, path.listFiles());
			    
			for (File testDir : outagedTestDirs) {
				try {
					FileUtils.forceDelete(testDir);
					log.info("Deleted temporary test access working directory: {}", testDir.getAbsolutePath());
				} catch (Exception e) {
					log.error("Failed to delete temporary test access working directory: {}", testDir.getAbsolutePath());
				}
			}
			log.info("Executed access tests cleanup task");
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
