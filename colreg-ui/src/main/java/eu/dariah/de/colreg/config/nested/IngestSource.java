package eu.dariah.de.colreg.config.nested;

import java.util.List;

import lombok.Data;

@Data
public class IngestSource {
	private String key;
	private String baseUrl;
	private String accessUrl;
	private String userEndpoint;
	private List<String> allowedAddresses;
}
