package eu.dariah.de.colreg.service;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import de.unibamberg.minf.dme.model.serialization.DatamodelContainer;
import eu.dariah.de.colreg.dao.vocabulary.EncodingSchemeDao;
import eu.dariah.de.colreg.model.vocabulary.EncodingScheme;
import reactor.core.publisher.Mono;

@Component
public class SchemaServiceImpl implements SchemaService {
	protected static final Logger logger = LoggerFactory.getLogger(SchemaServiceImpl.class);
		
	// This is merely for caching schemata once the SR is down
	// TODO: Two level cache to reduce traffic?!
	@Autowired private EncodingSchemeDao encodingSchemeDao;
	
	@Value("${api.dme.models}")
	private String fetchAllUrl;
	
	@Value("${api.dme.modelLink}")
	private String schemaLinkUrl;
		
	@Override
	public List<EncodingScheme> findAllSchemas() {
		return this.findAllSchemas(false);
	}
	
	@Override
	public List<EncodingScheme> findAllSchemas(boolean useCache) {
		if (useCache) {
			return encodingSchemeDao.findAll();
		}
		
		try {
			// TODO: Adapt to WebClient here
			//DatamodelContainer[] result = restTemplate.getForObject(fetchAllUrl, (new DatamodelContainer[0]).getClass());
			
			WebClient client = WebClient.builder()
					.baseUrl(fetchAllUrl)
					.defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
					.build();

			DatamodelContainer[] result = client.get().retrieve().bodyToMono((new DatamodelContainer[0]).getClass()).block();
			if (result==null) {
				return null;
			}

			List<EncodingScheme> s = new ArrayList<EncodingScheme>(result.length);
			EncodingScheme pojo;
			for (int i=0; i<result.length;i++) {
				pojo = new EncodingScheme();
				pojo.setId(result[i].getModel().getId());
				pojo.setUrl(String.format(schemaLinkUrl, result[i].getModel().getId()));
				pojo.setName(result[i].getModel().getName());
				
				s.add(pojo);
			}
			
			encodingSchemeDao.deleteAll();
			encodingSchemeDao.saveAll(s);
			return s;
		} catch (Exception e) {
			if (this.testAvailability()) {
				logger.warn(String.format("Error while fetching schemata from Schema Registry: [%s]", fetchAllUrl),e );
			}
			return encodingSchemeDao.findAll();
		}
	}
	
	private boolean testAvailability() {
		if (fetchAllUrl==null) {
			return false;
		}
		try {
			URL url = new URL(fetchAllUrl); 
			HttpURLConnection httpConnection;
			URLConnection urlConnection = url.openConnection();
			int httpStatusCode = 0;
			if (HttpURLConnection.class.isAssignableFrom(urlConnection.getClass())) {
				httpConnection = (HttpURLConnection)urlConnection;
				httpConnection.setInstanceFollowRedirects(false);
				httpStatusCode = httpConnection.getResponseCode();
				if (httpStatusCode!=200) {
					logger.warn("Established connection to DME resulted HTTP status code " + httpStatusCode);
					return false;
				}
			} else {
				return false;
			}
		} catch (Exception e) {
			logger.warn("Failed to connect to DME [{}]: {}", fetchAllUrl, e.getMessage());
			return false;
		}
		
		return true;
	}
}
