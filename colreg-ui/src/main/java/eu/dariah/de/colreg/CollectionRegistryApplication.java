package eu.dariah.de.colreg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan
public class CollectionRegistryApplication {	
	public static void main(String[] args) {
		SpringApplication.run(CollectionRegistryApplication.class, args);
	}
}