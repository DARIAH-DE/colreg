package eu.dariah.de.colreg.controller.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;

import de.unibamberg.minf.core.web.controller.BaseTranslationController;
import eu.dariah.de.colreg.service.CollectionService;
import eu.dariah.de.dariahsp.web.AuthInfoHelper;
import eu.dariah.de.dariahsp.web.model.AuthPojo;

public abstract class BaseController extends BaseTranslationController {
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public final String NAVIGATION_ELEMENT_ATTRIBUTE = "_navigationAttribute";
	
	@Autowired private CollectionService collectionService;
	@Autowired protected AuthInfoHelper authInfoHelper;

	public BaseController(String mainNavId) {
		super(mainNavId);
	}
	
	@ModelAttribute("_draftCount")
	public Long getDraftCount() {
		AuthPojo auth = authInfoHelper.getAuth();
		
		if (auth!=null && auth.isAuth()) {
			return collectionService.countDrafts(auth.getUserId()); 
		}
		
		return null;
	}
	
	

}
