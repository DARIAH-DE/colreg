package eu.dariah.de.colreg.controller.automation;

import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class TimeoutTimerTask extends TimerTask {
	protected static final Logger logger = LoggerFactory.getLogger(TimeoutTimerTask.class);
	
	private final Future<?> future;
	private final ExecutorService executor;
	
	public TimeoutTimerTask(Future<?> future, ExecutorService executor) {
		this.future = future;
		this.executor = executor;
	}
	
	@Override
	public void run() {
		if (!this.future.isDone()) {
			try {
				logger.debug("Executor timeout exceeded -> attempting to shutdown");
			    executor.awaitTermination(10, TimeUnit.SECONDS);
			} catch (InterruptedException e) {
				logger.debug("Executor timeout exceeded -> tasks interrupted");
			} finally {
			    if (!executor.isTerminated()) {
			    	logger.warn("Executor timeout exceeded -> canceling remaining tasks");
			    }
			    executor.shutdownNow();
			    logger.debug("Executor timeout exceeded -> executor is shut down");
			}
		}
	}
}
