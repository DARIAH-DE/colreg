package eu.dariah.de.colreg.controller.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.util.matcher.IpAddressMatcher;
import org.springframework.validation.DataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import eu.dariah.de.colreg.config.IngestConfig;
import eu.dariah.de.colreg.config.nested.IngestSource;
import eu.dariah.de.colreg.controller.base.BaseApiController;
import eu.dariah.de.colreg.model.Access;
import eu.dariah.de.colreg.model.Agent;
import eu.dariah.de.colreg.model.Collection;
import eu.dariah.de.colreg.model.Datamodel;
import eu.dariah.de.colreg.model.LocalizedDescription;
import eu.dariah.de.colreg.model.api.IngestResponse;
import eu.dariah.de.colreg.model.api.repository.RepositoryDraft;
import eu.dariah.de.colreg.model.api.repository.RepositoryDraftContainer;
import eu.dariah.de.colreg.model.marshalling.XMLConverter;
import eu.dariah.de.colreg.model.validation.CollectionValidator;
import eu.dariah.de.colreg.model.vocabulary.EncodingScheme;
import eu.dariah.de.colreg.model.vocabulary.License;
import eu.dariah.de.colreg.model.vocabulary.generic.VocabularyItem;
import eu.dariah.de.colreg.pojo.api.CollectionApiPojo;
import eu.dariah.de.colreg.pojo.api.ExtendedCollectionApiPojo;
import eu.dariah.de.colreg.pojo.api.results.CollectionApiResultPojo;
import eu.dariah.de.colreg.pojo.converter.api.CollectionApiConverter;
import eu.dariah.de.colreg.pojo.converter.api.ExtendedCollectionApiConverter;
import eu.dariah.de.colreg.service.AccessTypeService;
import eu.dariah.de.colreg.service.AccrualMethodService;
import eu.dariah.de.colreg.service.AccrualPeriodicityService;
import eu.dariah.de.colreg.service.AccrualPolicyService;
import eu.dariah.de.colreg.service.AgentService;
import eu.dariah.de.colreg.service.AgentTypeService;
import eu.dariah.de.colreg.service.CollectionService;
import eu.dariah.de.colreg.service.LicenseService;
import eu.dariah.de.colreg.service.SchemaService;
import eu.dariah.de.colreg.service.UserServiceImpl;
import eu.dariah.de.colreg.service.VocabularyItemService;
import eu.dariah.de.dariahsp.model.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(value="/api/collections")
public class CollectionApiController extends BaseApiController {	
	@Autowired private SchemaService schemaService;
	@Autowired private CollectionService collectionService;
	@Autowired private CollectionApiConverter collectionApiConverter;
	@Autowired private ExtendedCollectionApiConverter extendedCollectionApiConverter;
	
	@Autowired private AgentTypeService agentTypeService;
	@Autowired private AccessTypeService accessTypeService;
	@Autowired private AccrualMethodService accrualMethodService;
	@Autowired private AccrualPolicyService accrualPolicyService;
	@Autowired private AccrualPeriodicityService accrualPeriodicityService;
	@Autowired private AgentService agentService;
	
	@Autowired private LicenseService licenseService;
	@Autowired private VocabularyItemService vocabularyItemService;
	
	
	@Autowired private XMLConverter xmlConverter;
	@Autowired private UserServiceImpl userService;
	@Autowired private CollectionValidator collectionValidator;
	
	@Autowired private IngestConfig ingestConfig;
	
	@GetMapping(produces = { "application/json", "application/xml" })
	public @ResponseBody CollectionApiResultPojo<CollectionApiPojo> getAllPublic(@RequestParam(required=false) String locale) {
		CollectionApiResultPojo<CollectionApiPojo> result = new CollectionApiResultPojo<>("listCollections");
		Locale l = this.handleLocale(result, locale);
		if (!result.isSuccess()) {
			return result;
		}
			
		Map<String, String> agentTypeIdLabelMap = agentTypeService.findAgentTypeIdLabelMap();
		Map<String, Agent> agentIdMap = this.getAgentIdMap();
		
		List<Collection> collections = collectionService.findAllCurrent();
		List<CollectionApiPojo> collectionPojos = collectionApiConverter.convertToPojos(collections, l, agentIdMap, agentTypeIdLabelMap);
		
		result.setContent(collectionPojos);
		
		return result;
	}
	
	@GetMapping(value="/{collectionId}", produces = { "application/json", "application/xml" })
	public @ResponseBody CollectionApiResultPojo<ExtendedCollectionApiPojo> getCollection(@PathVariable String collectionId, @RequestParam(required=false) String locale) {
		CollectionApiResultPojo<ExtendedCollectionApiPojo> result = new CollectionApiResultPojo<>("getCollection");
		Locale l = this.handleLocale(result, locale);
		if (!result.isSuccess()) {
			return result;
		}
		
		Collection c = collectionService.findCurrentByCollectionId(collectionId);
		if (c==null) {
			result.setMessage("No collection matches specified ID.");
			result.setSuccess(false);
			return result;
		}
		
		Map<String, String> agentTypeIdLabelMap = agentTypeService.findAgentTypeIdLabelMap();
		Map<String, String> accessTypeIdLabelMap = accessTypeService.findAccessTypeIdLabelsMap(); 
		Map<String, String> accrualMethodIdIdentifierMap = accrualMethodService.findAccrualMethodIdIdentifierMap(); 
		Map<String, String> accrualPolicyIdIdentifierMap = accrualPolicyService.findAccrualPolicyIdIdentifierMap(); 
		Map<String, String> accrualPeriodicityIdIdentifierMap = accrualPeriodicityService.findAccrualPeriodicityIdIdentifierMap();
		
		Map<String, Agent> agentIdMap = this.getAgentIdMap();

		List<ExtendedCollectionApiPojo> contents = new ArrayList<>();
		contents.add(extendedCollectionApiConverter.convertToPojo(c, l, agentIdMap, agentTypeIdLabelMap, accessTypeIdLabelMap, accrualMethodIdIdentifierMap, accrualPolicyIdIdentifierMap, accrualPeriodicityIdIdentifierMap));
		
		result.setContent(contents);
		return result;
	}	
	
	@PostMapping(value="/submitDraft", produces="application/xml")
	public @ResponseBody IngestResponse postDraft(@RequestParam(required=false,name="key") String apiKey, @RequestBody String xml, HttpServletResponse response, HttpServletRequest request) {
		return this.ingest(apiKey, xml, false, response, request);
	}
	
	@PostMapping(value="/submitAndPublish", produces="application/xml")
	public @ResponseBody IngestResponse postDraftAndPublish(@RequestParam(required=false,name="key") String apiKey, @RequestBody String xml, HttpServletResponse response, HttpServletRequest request) {
		return this.ingest(apiKey, xml, true, response, request);
	}
	
	public @ResponseBody IngestResponse ingest(String apiKey, String xml, boolean publish, HttpServletResponse response, HttpServletRequest request) {
		IngestResponse resp = new IngestResponse();
		IngestSource verifiedSource = this.verifyIngestAuthentication(apiKey, request, resp);
		if (verifiedSource==null) {
			log.warn("API request blocked (ip filter: {}, key check: {}) - remote ip: {}, key: {}", 
					ingestConfig.isUseIpFilter() ? "ON" : "OFF", ingestConfig.isUseApiKey() ? "ON" : "OFF",
					request.getRemoteAddr(), apiKey==null ? "-" : apiKey);
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED); 
			return resp;
		}
				
		try {
			RepositoryDraft draft = parseProvidedDraft(resp, xml);
			if (draft==null) {
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				return resp;
			}
				
			Collection c = this.convertDraft(resp, draft);
			User u = this.getDraftUser(resp, verifiedSource, draft);
			if (c==null || u==null) {
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				return resp;
			}
			c.setDraftUserId(u.getId());

			if (!validateCollection(resp, c)) {
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				return resp;
			}
			collectionService.save(c, u.getId());
			// Save twice to simulate draft -> publish workflow
			if (publish) {
				c.setDraftUserId(null);
				collectionService.save(c, u.getId());
			}
			
			this.finalizeSuccessResponse(resp, c);
			response.setStatus(HttpServletResponse.SC_OK);
			
		} catch (Exception e) {
			resp.setStatus("Error");
			resp.setError("An unhandled exception occurred. Check server log.");
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			logger.error("Failed to consume draft", e);
		}
		return resp;
	}
	
	private void finalizeSuccessResponse(IngestResponse resp, Collection c) {
		if (c.getDraftUserId()==null) {
			resp.setUrl(ServletUriComponentsBuilder.fromCurrentContextPath().path("/collections/" + c.getEntityId()).build().toUriString());
			resp.setCollectionState("published");
		} else {
			resp.setUrl(ServletUriComponentsBuilder.fromCurrentContextPath().path("/drafts/" + c.getEntityId()).build().toUriString());
			resp.setCollectionState("draft");
		}
		resp.setCollectionId(c.getEntityId());
		resp.setStatus("OK");
		
		resp.setXml(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/collections/" + c.getEntityId() + ".xml").build().toUriString());
		resp.setJson(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/collections/" + c.getEntityId() + ".json").build().toUriString());
	}
	
	
	private boolean validateCollection(IngestResponse resp, Collection c) {
		DataBinder binder = new DataBinder(c);
		collectionValidator.validate(c, binder.getBindingResult());
		if (binder.getBindingResult().hasErrors()) {
			resp.setError("Validation errors occurred: " + binder.getBindingResult().toString());
			return false;
		}
		return true;
	}
	
	
	private Collection convertDraft(IngestResponse resp, RepositoryDraft draft) {
		Collection c = new Collection();
		
		// Titles, descriptions -> localized descriptions
		if (draft.getTitles()!=null) {
			c.setLocalizedDescriptions(new ArrayList<>());
			LocalizedDescription desc;
			for (int i=0; i<draft.getTitles().size(); i++) {
				desc = new LocalizedDescription();
				desc.setLanguageId("deu");
				desc.setTitle(draft.getTitles().get(i));
				if (draft.getDescriptions()!=null && draft.getDescriptions().size()>i) {
					desc.setDescription(draft.getDescriptions().get(i));
				}
				c.getLocalizedDescriptions().add(desc);
			}
		}
		
		Access a = new Access();
		a.setDatamodels(new ArrayList<>());
		a.setType(accessTypeService.findAccessTypeByIdentfier("oaipmh").getId());
		a.setUri(this.getOaiUrl(draft));
		a.setParam("set", this.getOaiSet(draft));
		a.setParam("metadataPrefix", "oai_dc");
		
		if (a.getUri()==null || a.getParam("set")==null) {
			resp.setError("Url and set for OAI-PMH could not be derived from identifier");
			return null;
		}
		
		
		String dmId = null;
		for (EncodingScheme es : schemaService.findAllSchemas()) {
			if (es.getName().equalsIgnoreCase("oai_dc")) {
				dmId = es.getId();
			}
		}
		
		if (dmId!=null) {
			Datamodel dm = new Datamodel();
			dm.setModelId(dmId);
			a.getDatamodels().add(dm);
		} else {
			resp.setError("Datamodel (oai_dc) could not be found and assigned; check DME attached to this instance");
			return null;
		}
		
		c.setAccessMethods(new ArrayList<>(1));
		c.getAccessMethods().add(a);
		
		// Identifiers -> Identifiers, Access
		this.removeOaiIdentifier(draft);
		c.setProvidedIdentifier(draft.getIdentifiers());
		
		License cc0license = licenseService.findLicenseByIdentifier("CC0");
		c.setCollectionDescriptionRights(cc0license!=null ? cc0license.getId() : null);
		c.setAccessRights(cc0license!=null ? cc0license.getId() : null);
		
		c.setCollectionTypes(new ArrayList<>());
		c.getCollectionTypes().add(this.getCollectionType().getIdentifier());
		
		c.setWebPage(draft.getAboutAttribute()!=null ? draft.getAboutAttribute() : a.getUri());
		
		return c;		
	}
	
	private VocabularyItem getCollectionType() {
		List<VocabularyItem> collectionTypes = vocabularyItemService.findVocabularyItems(Collection.COLLECTION_TYPES_VOCABULARY_IDENTIFIER);
		VocabularyItem type = null;
		for (VocabularyItem t : collectionTypes) {
			if (t.getIdentifier().equals("dariah-de-repository")) {
				type = t;
				break;
			}
		}
		if (type==null) {
			type = new VocabularyItem();
			type.setId("new");
			type.setIdentifier("dariah-de-repository");
			type.setDefaultName("DARIAH-DE Repository");
			type.setVocabularyIdentifier(Collection.COLLECTION_TYPES_VOCABULARY_IDENTIFIER);
			vocabularyItemService.saveVocabularyItem(type);
		}
		return type;
	}
	
	private User getDraftUser(IngestResponse resp, IngestSource verifiedSource, RepositoryDraft draft) {
		// Creator and draft user
		String username = null;
		String endpoint = verifiedSource.getUserEndpoint()==null ? "local" : verifiedSource.getUserEndpoint();
		if (draft.getContributors()!=null) {
			for (String contr : draft.getContributors()) {
				// TODO: This is something that could accidentally match a user-provided entry
				if (contr.toLowerCase().endsWith("@dariah.eu") || contr.toLowerCase().endsWith("@winseda.de")) {
					username = contr.toLowerCase();
					break;
				}
			}
		}
		
		if (username==null) {
			resp.setError("Could not reliably determine draft user");
			return null;
		}
		
		User u = userService.loadUserByUsername(endpoint, username);
		if (u==null) {
			u = new User();
			u.setUsername(username);
			u.setIssuer(endpoint);
			userService.saveUser(u);
		}
		return u;
	}
	
	private RepositoryDraft parseProvidedDraft(IngestResponse resp, String xml) {
		logger.info("Processing ingested draft: \n {} ", xml);
		
		if (xml==null || xml.isBlank()) {
			resp.setError("No request body provided");
			return null;
		}
		
		RepositoryDraft draft = null;
		try {
			RepositoryDraftContainer container = RepositoryDraftContainer.class.cast(xmlConverter.convertXmlToObject(xml));
			draft = container.getRepositoryDraft();
			if (draft==null) {
				resp.setError("No content provided in request body.");
			}
		} catch (Exception e) {
			resp.setError("Failed to parse provided payload: " + e.getMessage());
		}
		return draft;
	}
	
	private IngestSource verifyIngestAuthentication(String apiKey, HttpServletRequest request, IngestResponse resp) {
		if ((!ingestConfig.isUseApiKey() && !ingestConfig.isUseIpFilter()) || (ingestConfig.getSources()==null || ingestConfig.getSources().isEmpty())) {
			resp.setError("No service authorization method configured for this CR instance");
			return null;
		}
		
		// API Key handling
		List<IngestSource> qualSources = this.verifyApiKey(apiKey, resp);
		if (qualSources.isEmpty()) {
			return null;
		}
		
		// IP address verification
		qualSources = this.verifyRemoteIp(qualSources, request, resp);
		if (qualSources.isEmpty()) {
			return null;
		}
		
		if (qualSources.size()>1) {
			logger.warn("Multiple ingest sources matching the request [{}, {}]", request.getRemoteAddr(), apiKey);
		}
		return qualSources.get(0);	
	}
	
	private List<IngestSource> verifyApiKey(String apiKey, IngestResponse resp) {
		if (ingestConfig.isUseApiKey() && apiKey==null) {
			resp.setError("API key verification is configured for this CR instance; please provide one (key=...) with every request");
			return new ArrayList<>(0);
		} 
		if (ingestConfig.isUseApiKey()) {
			List<IngestSource> qualSources = ingestConfig.getSources().stream()
				.filter(s -> s.getKey()!=null && s.getKey().equalsIgnoreCase(apiKey))
				.distinct()
				.collect(Collectors.toList());
			if (qualSources.isEmpty()) {
				resp.setError("Provided API key is not known to this CR instance");
			}
			return qualSources;
		} else {
			return ingestConfig.getSources();
		}
	}
	
	private List<IngestSource> verifyRemoteIp(List<IngestSource> qualSources, HttpServletRequest request, IngestResponse resp) {
		if (ingestConfig.isUseIpFilter()) {
			qualSources = qualSources.stream()
				.filter(s -> s.getAllowedAddresses()!=null && getIsIpMatch(request, s.getAllowedAddresses()))
				.collect(Collectors.toList());
			if (qualSources.isEmpty()) {
				resp.setError("Remote address unknown or does not match specified API key (if provided)");
			}
		}
		return qualSources;
	}
	
	
	private boolean getIsIpMatch(HttpServletRequest request, List<String> ipPatterns) {
		IpAddressMatcher ipAddressMatcher;
		for (String pattern : ipPatterns) {
			ipAddressMatcher = new IpAddressMatcher(pattern);
			if (ipAddressMatcher.matches(request)) {
				return true;
			}
		}
		return false;
	}
	
	private String getOaiUrl(RepositoryDraft draft) {
		if (draft==null || draft.getIdentifiers()==null || draft.getIdentifiers().isEmpty()) {
			return null;
		}
		for (String id : draft.getIdentifiers()) {
			if (id.startsWith("http") && id.contains("oai")) {
				return id.substring(0, id.lastIndexOf("/")+1);
			}
		}
		return null;
	}
	
	private void removeOaiIdentifier(RepositoryDraft draft) {
		if (draft==null || draft.getIdentifiers()==null || draft.getIdentifiers().isEmpty()) {
			return;
		}
		draft.getIdentifiers().removeIf(id -> id.startsWith("http") && id.contains("oai"));
	}
	
	private String getOaiSet(RepositoryDraft draft) {
		if (draft==null || draft.getIdentifiers()==null || draft.getIdentifiers().isEmpty()) {
			return null;
		}
		for (String id : draft.getIdentifiers()) {
			if (id.startsWith("http") && id.contains("oai")) {
				return id.substring(id.lastIndexOf("/")+1);
			}
		}
		return null;
	}
	
	private Map<String, Agent> getAgentIdMap() {
		Map<String, Agent> agentIdMap = new HashMap<String, Agent>();
		for (Agent a : agentService.findAllCurrent()) {
			agentIdMap.put(a.getEntityId(), a);
		}
		return agentIdMap;
	} 
}
