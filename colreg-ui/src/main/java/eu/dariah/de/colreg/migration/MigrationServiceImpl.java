package eu.dariah.de.colreg.migration;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.FileAttribute;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.ResourcePatternUtils;
import org.springframework.data.mongodb.core.CollectionCallback;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

import de.unibamberg.minf.dme.model.version.VersionInfo;
import de.unibamberg.minf.dme.model.version.VersionInfoImpl;
import eu.dariah.de.colreg.config.MigratedConfig;
import eu.dariah.de.colreg.dao.VersionDao;
import eu.dariah.de.colreg.dao.base.DaoImpl;
import eu.dariah.de.colreg.dao.vocabulary.generic.VocabularyDao;
import eu.dariah.de.colreg.dao.vocabulary.generic.VocabularyItemDao;
import eu.dariah.de.colreg.model.Collection;
import eu.dariah.de.colreg.model.CollectionAgentRelation;
import eu.dariah.de.colreg.model.CollectionRelation;
import eu.dariah.de.colreg.model.vocabulary.AccessType;
import eu.dariah.de.colreg.model.vocabulary.AccrualMethod;
import eu.dariah.de.colreg.model.vocabulary.AccrualPeriodicity;
import eu.dariah.de.colreg.model.vocabulary.AccrualPolicy;
import eu.dariah.de.colreg.model.vocabulary.generic.Vocabulary;
import eu.dariah.de.colreg.model.vocabulary.generic.VocabularyItem;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class MigrationServiceImpl implements MigrationService {
	private static final String VERSION_HASH_PREFIX = "CollectionRegistry";
	private static final Pattern LOCALIZABLE_ENTITY_IDENTIFIER_PATTERN = Pattern.compile("([A-Za-z0-9-_])+");
	
	@Autowired private MigratedConfig appConfig;
	@Autowired private MongoTemplate mongoTemplate;
	@Autowired private ObjectMapper objectMapper;
	
	@Autowired private VersionDao versionDao;
	@Autowired private VocabularyDao vocabularyDao;
	@Autowired private VocabularyItemDao vocabularyItemDao;
	
	@Autowired private ResourceLoader resourceLoader;
	
	private final MessageDigest md;
	
	public MigrationServiceImpl() throws NoSuchAlgorithmException {
		md = MessageDigest.getInstance("MD5");
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		List<String> versions = new ArrayList<>();
		List<VersionInfo> versionInfos = versionDao.findAll();
		for (VersionInfo vi : versionInfos) {
			if (!vi.getVersionHash().equals(new String(md.digest(new String(VERSION_HASH_PREFIX + vi.getVersion()).getBytes(StandardCharsets.UTF_8)), StandardCharsets.UTF_8))) {
				log.error("Cancelling migration checks: failed to compare version hashes. Is the correct database configured?");
				return;
			}
			versions.add(vi.getVersion());
		}
		this.performMigrations(versions);
	}
	
	private void performMigrations(List<String> existingVersions) throws Exception {
		boolean backedUp = false;
		
		backedUp = backedUp | this.migrate(existingVersions, !backedUp, "1.0", this::importInitializationData);
		backedUp = backedUp | this.migrate(existingVersions, !backedUp, "3.8", this::migrateImages);
		backedUp = backedUp | this.migrate(existingVersions, !backedUp, "3.8.1", this::createCollectionTypesVocabulary);
		backedUp = backedUp | this.migrate(existingVersions, !backedUp, "3.8.2", this::migrateCollectionTypes);
		backedUp = backedUp | this.migrate(existingVersions, !backedUp, "3.9.0", this::createItemTypesVocabulary);
		backedUp = backedUp | this.migrate(existingVersions, !backedUp, "3.9.1", this::migrateItemTypes);
		backedUp = backedUp | this.migrate(existingVersions, !backedUp, "3.9.2", this::migrateCollectionRelations);
		backedUp = backedUp | this.migrate(existingVersions, !backedUp, "3.9.3", this::migrateAgentRelationTypes);
		backedUp = backedUp | this.migrate(existingVersions, !backedUp, "3.12.0", this::migrateAccessSchemeIdToMap);
		backedUp = backedUp | this.migrate(existingVersions, !backedUp, "3.16.0", this::addGitAccessType);
		backedUp = backedUp | this.migrate(existingVersions, !backedUp, "4.3", this::migrateAccessParams);
		backedUp = backedUp | this.migrate(existingVersions, !backedUp, "4.5.14", this::migrateAccrualIdentifiers);
		
		log.info("Backup performed: {}", backedUp);
	}
		
	private boolean migrate(List<String> existingVersions, boolean backup, String version, MigrationAction migration) throws Exception {
		if ((version.equals("1.0")&&existingVersions.isEmpty()) || (!version.equals("1.0") && !existingVersions.contains(version))) {
			log.info("Migrating to version [{}]", version);	
			if (backup) {
				this.backupDb();
			}
			boolean success = migration.migrate();
			this.saveVersionInfo(version, success);
			log.info("Migration to version [{}] performed {}", version, success ? "sucessfully" : "WITH ERRORS");		
			return true;
		}
		return false;
	}
	
	private boolean importInitializationData() {
		try {
			log.info("Importing collection registry initialization data");
			String dbCollection;
			java.util.Collection<Document> docs;
			JsonNode n;
			
			for (Resource r : ResourcePatternUtils.getResourcePatternResolver(resourceLoader).getResources("classpath:initialization_data/*.json")) {
				if (r.getFilename()==null) {
					log.warn("Failed to access resource in initialization_data");
					continue;
				}
				dbCollection = r.getFilename().substring(0, r.getFilename().lastIndexOf('.'));
				docs = new ArrayList<>();
				n = objectMapper.readTree(r.getInputStream());
				if (n.isArray()) {	
					for (int i=0; i<n.size(); i++) {
						docs.add(Document.parse(n.get(i).toString())); 
					}
				} else {
					docs.add(Document.parse(n.toString()));
				}
				docs = mongoTemplate.insert(docs, dbCollection);
				log.info("Imported {} documents to {}", docs.size(), dbCollection);
			}	
		} catch (Exception e) {
			log.error("Failed to initialize database", e);
			return false;
		}
		return true;  
	}
	
	private boolean migrateAccessSchemeIdToMap() {
		ObjectNode objectNode, datamodelNode;
		ArrayNode accessMethodsNode, schemeIdsNode, datamodelsNode;
		
		for (String rawCollection : this.getObjectsAsString("collection")) {
			try {
 				objectNode = (ObjectNode)objectMapper.readTree(rawCollection);
				if (objectNode.get("accessMethods")==null || objectNode.get("accessMethods").isMissingNode()) {
					continue;
				}
				boolean save = false;
				accessMethodsNode = (ArrayNode)objectNode.get("accessMethods");
				for (JsonNode accessMethodNode : accessMethodsNode) {
					if (accessMethodNode.get("schemeIds")==null || accessMethodNode.get("schemeIds").isMissingNode()) {
						continue;
					}
					schemeIdsNode = (ArrayNode)accessMethodNode.get("schemeIds");
					datamodelsNode = objectMapper.createArrayNode();
					for (JsonNode schemeIdNode : schemeIdsNode) {
						datamodelNode = objectMapper.createObjectNode();
						datamodelNode.put("modelId", schemeIdNode.asText());
						datamodelsNode.add(datamodelNode);
					}
					save = true;
					((ObjectNode)accessMethodNode).set("datamodels", datamodelsNode);
					((ObjectNode)accessMethodNode).remove("schemeIds");
				}
				if (save) {
					mongoTemplate.save(objectNode.toString(), DaoImpl.getCollectionName(Collection.class));
				}
			} catch (Exception e) {
				log.error("Failed to update database", e);
				return false;
			}
		}
		return true;  
	}

	private boolean migrateAgentRelationTypes() {
		this.createVocabulary(CollectionAgentRelation.AGENT_RELATION_TYPES_VOCABULARY_IDENTIFIER, "Agent Relation Types");
				
		VocabularyItem vi = new VocabularyItem();
		vi.setIdentifier("owner");
		vi.setDefaultName("Owner");
		vi.setVocabularyIdentifier(CollectionAgentRelation.AGENT_RELATION_TYPES_VOCABULARY_IDENTIFIER);
		vocabularyItemDao.save(vi);
		
		vi = new VocabularyItem();
		vi.setIdentifier("creator");
		vi.setDefaultName("Creator");
		vi.setVocabularyIdentifier(CollectionAgentRelation.AGENT_RELATION_TYPES_VOCABULARY_IDENTIFIER);
		vocabularyItemDao.save(vi);
		
		vi = new VocabularyItem();
		vi.setIdentifier("sponsor");
		vi.setDefaultName("Sponsor");
		vi.setVocabularyIdentifier(CollectionAgentRelation.AGENT_RELATION_TYPES_VOCABULARY_IDENTIFIER);
		vocabularyItemDao.save(vi);
		
		
		ObjectNode objectNode;
		ArrayNode relationsNode, relationTypesNode, setRelationTypesNode;
		for (String rawCollection : this.getObjectsAsString("collection")) {
			try {
 				objectNode = (ObjectNode)objectMapper.readTree(rawCollection);
				if (objectNode.get("agentRelations")==null || objectNode.get("agentRelations").isMissingNode()) {
					continue;
				}
				boolean save = false;
				relationsNode = (ArrayNode)objectNode.get("agentRelations");
				for (JsonNode relationNode : relationsNode) {
					if (relationNode.get("typeIds")==null || relationNode.get("typeIds").isMissingNode()) {
						continue;
					}
					relationTypesNode = (ArrayNode)relationNode.get("typeIds");
					setRelationTypesNode = objectMapper.createArrayNode();
					for (JsonNode relationTypeNode : relationTypesNode) {
						if (relationTypeNode.asText().equals("56bf6c17e4b0750deb67b1f8")) {
							setRelationTypesNode.add("owner");
						} else if (relationTypeNode.asText().equals("56bf6c17e4b0750deb67b1ff")) {
							setRelationTypesNode.add("creator");
						} else if (relationTypeNode.asText().equals("5a37c39cff239628221ba091")) {
							setRelationTypesNode.add("sponsor");
						} else {
							throw new Exception("Unknown agent relation type id; migrate manually");
						}
					}
					save = true;
					((ObjectNode)relationNode).set("typeIds", setRelationTypesNode);
				}
				if (save) {
					mongoTemplate.save(objectNode.toString(), DaoImpl.getCollectionName(Collection.class));
				}
			} catch (Exception e) {
				log.error("Failed to update database", e);
				return false;
			}
		}
		
		mongoTemplate.dropCollection("agentRelationType");
		return true;  
	}

	private boolean migrateCollectionRelations() {
		this.createVocabulary(CollectionRelation.COLLECTION_RELATION_TYPES_VOCABULARY_IDENTIFIER, "Collection Relation Types");
		
		// Existing hierarchical relations
		VocabularyItem vi = new VocabularyItem();
		vi.setIdentifier("childOf");
		vi.setDefaultName("is child of");
		vi.setVocabularyIdentifier(CollectionRelation.COLLECTION_RELATION_TYPES_VOCABULARY_IDENTIFIER);
		vocabularyItemDao.save(vi);
		
		// generic 'related to'
		vi = new VocabularyItem();
		vi.setIdentifier("relatedTo");
		vi.setDefaultName("is related to");
		vi.setVocabularyIdentifier(CollectionRelation.COLLECTION_RELATION_TYPES_VOCABULARY_IDENTIFIER);
		vocabularyItemDao.save(vi);
		
		ObjectNode objectNode;
		Map<String, ObjectNode> rawCollectionsIdMap = new HashMap<String, ObjectNode>();
		for (String rawCollection : this.getObjectsAsString("collection")) {
			try {
				objectNode = (ObjectNode)objectMapper.readTree(rawCollection);
				// We only update the latest versions of the collections for simplicity reasons
				if (objectNode.get("succeedingVersionId")==null || objectNode.get("succeedingVersionId").isMissingNode()
						 || objectNode.get("succeedingVersionId").asText().trim().isEmpty()) {
					rawCollectionsIdMap.put(objectNode.path("entityId").asText(), objectNode);
				}
			} catch (Exception e) {
				log.error("Failed to update database", e);
				return false;
			}
		}
		
		String parentCollectionId;		
		ObjectNode targetNode, relationNode, idNode;
		try {
			for (String collectionId : rawCollectionsIdMap.keySet()) {
				objectNode = rawCollectionsIdMap.get(collectionId);
				if (objectNode.get("parentCollectionId")==null || objectNode.get("parentCollectionId").isMissingNode()) {
					continue;
				}
				parentCollectionId = objectNode.get("parentCollectionId").textValue();
				if (parentCollectionId.trim().isEmpty()) {
					continue;
				}
				objectNode.remove("parentCollectionId");
				
				idNode = objectMapper.createObjectNode();
				idNode.put("$oid", new ObjectId().toString());
				
				relationNode = objectMapper.createObjectNode();
				relationNode.set("_id", idNode);
				relationNode.put("sourceEntityId", collectionId);
				relationNode.put("targetEntityId", parentCollectionId);
				relationNode.put("relationTypeId", "childOf");
				relationNode.put("bidirectional", false);
				
				this.appendRelationToCollection(objectNode, relationNode);
				mongoTemplate.save(objectNode.toString(), DaoImpl.getCollectionName(Collection.class));
				
				targetNode = rawCollectionsIdMap.get(parentCollectionId);
				this.appendRelationToCollection(targetNode, relationNode);
				mongoTemplate.save(targetNode.toString(), DaoImpl.getCollectionName(Collection.class));
			}
			return true;
		} catch (Exception e) {
			log.error("Failed to update database", e);
			return false;
		}
	}
	
	private boolean appendRelationToCollection(ObjectNode collectionNode, ObjectNode relationNode) {
		ArrayNode relationsNode;
		if (collectionNode.get("relations")!=null && !collectionNode.get("relations").isMissingNode()) {
			relationsNode = (ArrayNode)collectionNode.get("relations");
		} else {
			relationsNode = objectMapper.createArrayNode();
		}
		relationsNode.add(relationNode);
		collectionNode.set("relations", relationsNode);
		return true;
	}
	
	private boolean createCollectionTypesVocabulary() {
		return this.createVocabulary(Collection.COLLECTION_TYPES_VOCABULARY_IDENTIFIER, "Collection Types");
	}
	
	private boolean createItemTypesVocabulary() {
		return this.createVocabulary(Collection.ITEM_TYPES_VOCABULARY_IDENTIFIER, "Item Types");
	}

	private boolean createVocabulary(String identifier, String displayName) {
		List<Vocabulary> vocabularies = vocabularyDao.findAll();
		for (Vocabulary v : vocabularies) {
			if (v.getIdentifier().equals(identifier)) {
				log.warn(String.format("Vocabulary [%s] exists despite db version not contained; consider updating manually", identifier));
				return true;
			}
		}
		
		Vocabulary v = new Vocabulary();
		v.setIdentifier(identifier);
		v.setDefaultName(displayName);
		
		try {
			vocabularyDao.save(v);
			return true;
		} catch (Exception e) {
			log.error("Vocabulary migration failed", e);
			return false;
		}
	}
		
	private boolean migrateItemTypes() {
		try {
			Map<String, String> itemTypesMap = this.createItemTypes();
			this.updateCollectionsItemTypes(itemTypesMap);
			mongoTemplate.dropCollection("itemType");
			return true;
		} catch (Exception e) {
			log.error("Failed to update database", e);
			return false;
		}
	}
	
	private Map<String, String> createItemTypes() throws Exception {
		JsonNode node;
		ObjectNode objectNode;
		String identifier, label;
		Matcher m;
		
		VocabularyItem vi;
		
		Map<String, String> itemTypesMap = new HashMap<String, String>();
		
		List<String> rawItemTypes = this.getObjectsAsString("itemType");
		try {
			for (String rawItemType : rawItemTypes) {
				node = objectMapper.readTree(rawItemType);
				objectNode = (ObjectNode)node;
				
				label = objectNode.path("label").textValue();
				m = LOCALIZABLE_ENTITY_IDENTIFIER_PATTERN.matcher(label);
				
				identifier = "";
				while(m.find()) {
					identifier += m.group(0);
		        }
				identifier = identifier.substring(0, 1).toLowerCase() + identifier.substring(1); 
				
				vi = new VocabularyItem();
				vi.setIdentifier(identifier);
				vi.setExternalIdentifier(node.path("identifier").isMissingNode() ? "" : node.path("identifier").textValue());
				vi.setDefaultName(node.path("label").isMissingNode() ? "" : node.path("label").textValue());
				vi.setDescription(node.path("description").isMissingNode() ? "" : node.path("description").textValue());
				vi.setVocabularyIdentifier(Collection.ITEM_TYPES_VOCABULARY_IDENTIFIER);
				
				vocabularyItemDao.save(vi);
	
				itemTypesMap.put(node.path("_id").path("$oid").asText(), identifier);
				
			}
		} catch (Exception e) {
			log.error("Failed to update database", e);
			throw e;
		}
		return itemTypesMap;
	}
	
	private void updateCollectionsItemTypes(Map<String, String> itemTypesMap) throws Exception {
		List<String> rawCollections = this.getObjectsAsString("collection");
		
		JsonNode node;
		ObjectNode objectNode;
		ArrayNode itemTypesNode, itemTypeIdsNode;
		
		try {
			for (String rawCollection : rawCollections) {
			
				node = objectMapper.readTree(rawCollection);
				objectNode = (ObjectNode)node;
				if (objectNode.get("itemTypeIds")==null || objectNode.get("itemTypeIds").isMissingNode()) {
					continue;
				}
				itemTypesNode = objectMapper.createArrayNode();
				itemTypeIdsNode = (ArrayNode)objectNode.get("itemTypeIds");
				
				for (JsonNode itemTypeIdNode : itemTypeIdsNode) {
					if (itemTypesMap.containsKey(itemTypeIdNode.textValue())) {
						itemTypesNode.add(itemTypesMap.get(itemTypeIdNode.textValue()));
					} else {
						throw new Exception("Failed to resolve itemType ID; database might be corrupt: restore backup and migrate manually");
					}
				}
				
				objectNode.set("itemTypes", itemTypesNode);
				objectNode.remove("itemTypeIds");
				
				mongoTemplate.save(objectNode.toString(), DaoImpl.getCollectionName(Collection.class));
			}
		} catch (Exception e) {
			log.error("Failed to update database", e);
			throw e;
		}
	}
	
	
	private boolean migrateAccrualIdentifiers() {
		return this.migrateAccrualIdentifiers("accrualMethod", AccrualMethod.class) &&
				this.migrateAccrualIdentifiers("accrualPolicy", AccrualPolicy.class) &&
				this.migrateAccrualIdentifiers("accrualPeriodicity", AccrualPeriodicity.class);
	}
	
	private boolean migrateAccrualIdentifiers(String queryObject, Class<?> daoClass) {
		ObjectNode objectNode;
		
		for (String rawDocument : this.getObjectsAsString(queryObject)) {
			try {
 				objectNode = (ObjectNode)objectMapper.readTree(rawDocument);
				if (objectNode.get("identifier")!=null && objectNode.get("identifier").isTextual()) {
					objectNode.set("identifier", TextNode.valueOf(objectNode.get("identifier").asText().trim()));
					mongoTemplate.save(objectNode.toString(), DaoImpl.getCollectionName(daoClass));
				}
			} catch (Exception e) {
				log.error("Failed to update database", e);
				return false;
			}
		}
		return true;  
	}
	
	
	
	private boolean migrateAccessParams() {
		ObjectNode objectNode;
		ObjectNode paramNode;
		ArrayNode accessMethodsNode;
		ArrayNode paramsNode;
		
		for (String rawCollection : this.getObjectsAsString("collection")) {
			try {
 				objectNode = (ObjectNode)objectMapper.readTree(rawCollection);
				if (objectNode.get("accessMethods")==null || objectNode.get("accessMethods").isMissingNode()) {
					continue;
				}
				
				accessMethodsNode = (ArrayNode)objectNode.get("accessMethods");
				for (JsonNode accessMethodNode : accessMethodsNode) {
					if ( (accessMethodNode.get("oaiSet")==null || accessMethodNode.get("oaiSet").isMissingNode()) && 
						(accessMethodNode.get("metadataPrefix")==null || accessMethodNode.get("metadataPrefix").isMissingNode()) ){
						continue;
					}
					paramsNode = objectMapper.createArrayNode();
					if (accessMethodNode.get("oaiSet")!=null && !accessMethodNode.get("oaiSet").isMissingNode() && !accessMethodNode.get("oaiSet").asText().isEmpty()) {
						paramNode = objectMapper.createObjectNode();
						// OAI-PMH
						if (accessMethodNode.get("type").asText().equals("56bf6c17e4b0750deb67b1f3")) {
							paramNode.put("key", "set");
						} else {
							paramNode.put("key", "branch");
						}
						paramNode.put("value", accessMethodNode.get("oaiSet").asText());
						paramsNode.add(paramNode);
					}
					if (accessMethodNode.get("metadataPrefix")!=null && !accessMethodNode.get("metadataPrefix").isMissingNode() && !accessMethodNode.get("metadataPrefix").asText().isEmpty()) {
						paramNode = objectMapper.createObjectNode();
						paramNode.put("key", "metadataPrefix");
						paramNode.put("value", accessMethodNode.get("metadataPrefix").asText());
						paramsNode.add(paramNode);
					}
					((ObjectNode)accessMethodNode).set("params", paramsNode);
					((ObjectNode)accessMethodNode).remove("metadataPrefix");
					((ObjectNode)accessMethodNode).remove("oaiSet");
					
					mongoTemplate.save(objectNode.toString(), DaoImpl.getCollectionName(Collection.class));
				}
			} catch (Exception e) {
				log.error("Failed to update database", e);
				return false;
			}
		}
		return true;  
	}
	
	private boolean migrateCollectionTypes() {
		List<String> rawCollections = this.getObjectsAsString("collection");
		boolean errors = false;
		JsonNode node;
		ObjectNode objectNode;
		ArrayNode collectionTypesNode;
		
		List<String> collectionTypeIdentifiers = new ArrayList<String>();
		String identifier, label;
		Matcher m;
		
		for (String rawCollection : rawCollections) {
			try {
				node = objectMapper.readTree(rawCollection);
				// Skip drafts
				if (!node.path("draftUserId").isMissingNode() || !node.path("collectionType").isMissingNode()) {
					objectNode = (ObjectNode)node;
					label = objectNode.path("collectionType").textValue();
					m = LOCALIZABLE_ENTITY_IDENTIFIER_PATTERN.matcher(label);
					
					identifier = "";
					while(m.find()) {
						identifier += m.group(0);
			        }
					identifier = identifier.substring(0, 1).toLowerCase() + identifier.substring(1); 

					collectionTypesNode = objectMapper.createArrayNode();
					if (collectionTypeIdentifiers.contains(identifier)) {
						collectionTypeIdentifiers.add(identifier);
					} else {
						VocabularyItem vi = new VocabularyItem();
						vi.setDefaultName(label);
						vi.setIdentifier(identifier);
						vi.setVocabularyIdentifier(Collection.COLLECTION_TYPES_VOCABULARY_IDENTIFIER);
						
						vocabularyItemDao.save(vi);
						collectionTypeIdentifiers.add(identifier);
					}
					collectionTypesNode.add(identifier);
					objectNode.set("collectionTypes", collectionTypesNode);
					objectNode.remove("collectionType");
					
					mongoTemplate.save(objectNode.toString(), DaoImpl.getCollectionName(Collection.class));
				}
			} catch (Exception e) {
				log.error("Failed to update database", e);
				errors = true;
			}
		}
		return !errors;
	}
	
	private boolean migrateImages() {
		List<String> rawCollections = this.getObjectsAsString("collection");
		boolean errors = false;

		JsonNode node;
		ObjectNode objectNode, imagesNode;
		String imageId;
		for (String rawCollection : rawCollections) {
			try {
				node = objectMapper.readTree(rawCollection);
				/* - Convert image id under 'collectionImage' to map, 
				 * - set with collection object and
				 * - save */
				if (!node.path("collectionImage").isMissingNode()) {
					objectNode = (ObjectNode)node;
					
					imageId = objectNode.path("collectionImage").textValue();
				
					imagesNode = objectMapper.createObjectNode();
					imagesNode.put("0", imageId);
					
					objectNode.set("collectionImages", imagesNode);
					objectNode.remove("collectionImage");
					
					mongoTemplate.save(objectNode.toString(), DaoImpl.getCollectionName(Collection.class));
				}
			} catch (Exception e) {
				log.error("Failed to update database", e);
				errors = true;
			}
		}
		return !errors;
	}
	
	private boolean addGitAccessType() {
		boolean errors = false;
		try {
			AccessType a = new AccessType();
			a.setIdentifier("git");
			a.setLabel("Git Repository");
			a.setDescription("Collection provides resources though Git repository");
			a.setMachineAccessible(true);
			a.setCanUsePatterns(true);
			mongoTemplate.save(a, DaoImpl.getCollectionName(AccessType.class));
		} catch (Exception e) {
			log.error("Failed to update database to version 3.16.0", e);
			errors = true;
		}
		return !errors;
	}
	
	
	private void backupDb() throws Exception {
		String backupPath = appConfig.getPaths().getBackups() + File.separator + DateTime.now().toString(DateTimeFormat.forPattern("yyyyMMdd_HHmmss"));
		Files.createDirectories(Paths.get(new File(backupPath).toURI()), new FileAttribute<?>[0]);
		
		try {
			Runtime.getRuntime().exec(String.format("mongodump --out %s --db %s", backupPath, mongoTemplate.getDb().getName()));
			log.info("Backed up database {} to [{}]", mongoTemplate.getDb().getName(), backupPath);
		} catch (Exception e) {
			log.error("Failed to create mongodb backup", e);
			throw e;
		}
	}
	
	private void saveVersionInfo(String version, boolean success) {
		VersionInfo vi = new VersionInfoImpl();
		vi.setUpdateWithErrors(!success);
		vi.setVersion(version);		
		vi.setVersionHash(new String(md.digest(new String(VERSION_HASH_PREFIX + vi.getVersion()).getBytes(StandardCharsets.UTF_8)), StandardCharsets.UTF_8));
		
		versionDao.save(vi);
	}
	
	private List<String> getObjectsAsString(String queryObject) {
		return mongoTemplate.execute(queryObject, new CollectionCallback<List<String>>() {
			public List<String> doInCollection(MongoCollection<Document> collection) {
				MongoCursor<Document> cursor = collection.find().cursor();
				List<String> result = new ArrayList<>();
				while (cursor.hasNext()) {
					result.add(cursor.next().toJson());
				}
				return result;
			}
		});
	}
}
