package eu.dariah.de.colreg.config.nested;

import lombok.Data;

@Data
public class ThumbnailsConfigProperties {
	private int width = 150;
	private int height = 150;
}