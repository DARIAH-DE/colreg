package eu.dariah.de.colreg.pojo.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.stereotype.Component;

import eu.dariah.de.colreg.model.Access;
import eu.dariah.de.colreg.model.AccessParam;
import eu.dariah.de.colreg.model.Datamodel;
import eu.dariah.de.colreg.pojo.AccessParamPojo;
import eu.dariah.de.colreg.pojo.AccessPojo;
import eu.dariah.de.colreg.pojo.DatamodelPojo;
import eu.dariah.de.colreg.pojo.converter.base.BaseConverter;

@Component
public class AccessConverter extends BaseConverter<Access, AccessPojo> {

	@Override
	public AccessPojo convertToPojo(Access access, Locale locale) {
		return this.convertToPojo(access, locale, null);
	}
	
	public List<AccessPojo> convertToPojos(List<Access> access, Locale locale, Map<String, String> accessTypeIdLabelMap) {
		if (access==null || access.isEmpty()) {
			return new ArrayList<AccessPojo>(0);
		}
		List<AccessPojo> pojos = new ArrayList<AccessPojo>(access.size());
		for (Access a : access) {
			pojos.add(this.convertToPojo(a, locale, accessTypeIdLabelMap));
		}
		return pojos;
	}
	
	public AccessPojo convertToPojo(Access access, Locale locale, Map<String, String> accessTypeIdLabelMap) {
		AccessPojo pojo = null;
		if (access!=null) {
			pojo = new AccessPojo();
			
			if (access.getDatamodels()!=null) {
				
				pojo.setDatamodels(new ArrayList<>());
				DatamodelPojo dmPojo;
				for (Datamodel dm : access.getDatamodels()) {
					dmPojo = new DatamodelPojo();
					dmPojo.setId(dm.getModelId());
					dmPojo.setAlias(dm.getAlias());
					
					pojo.getDatamodels().add(dmPojo);
				}
				
			}
			if (accessTypeIdLabelMap!=null) {
				pojo.setType(accessTypeIdLabelMap.get(access.getType()));
			}
			pojo.setSubtype(access.getSubtype());
			pojo.setUri(access.getUri());
			pojo.setPatterns(access.getPatterns());
			pojo.setAccessModelId(access.getAccessModelId());
			pojo.setMethod(access.getMethod());
			
			if (access.getHeaders()!=null) {
				pojo.setHeaders(new ArrayList<>());
				AccessParamPojo pPojo;
				for (AccessParam p : access.getHeaders()) {
					pPojo = new AccessParamPojo();
					pPojo.setKey(p.getKey());
					pPojo.setValue(p.getValue());
					pojo.getHeaders().add(pPojo);
				}
			}
		
			
			if (access.getParams()!=null) {
				pojo.setParams(new ArrayList<>());
				AccessParamPojo pPojo;
				for (AccessParam p : access.getParams()) {
					pPojo = new AccessParamPojo();
					pPojo.setKey(p.getKey());
					pPojo.setValue(p.getValue());
					pojo.getParams().add(pPojo);
				}
			}
		}
		return pojo;
	}
}
