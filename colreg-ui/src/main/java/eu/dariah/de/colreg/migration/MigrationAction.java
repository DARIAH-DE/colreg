package eu.dariah.de.colreg.migration;

public interface MigrationAction {
	public boolean migrate();
}
