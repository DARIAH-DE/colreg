<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<sf:form method="POST" action="${saveUrl}">
	<div class="form-header">
		<h2 id="form-header-title"><s:message code="~eu.dariah.de.colreg.view.access.test" /></h2>
		<input type="hidden" id="sid" name="sid" value="${sid}" />
	</div>
	<div class="form-content">
		<fieldset>
			<div id="access-test-messages" class="form-group row">
			</div>
		</fieldset>
	</div>
	<div class="form-footer control-group">
		<div class="controls">
			<button class="btn cancel form-btn-cancel" type="reset"><i class="icon-ban-circle icon-black"></i><span><s:message code="~eu.dariah.de.colreg.common.actions.close" /></span></button>
		</div>
	</div>
</sf:form>