<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<h2><s:message code="~eu.dariah.de.colreg.titles.editor_options" /></h2>
<div class="checkbox">
	<label>
		<input class="chk-toggle-hints" type="checkbox" value="" />
		<s:message code="~eu.dariah.de.colreg.view.common.labels.show_hints" />
	</label>
</div>