<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<nav class="sidebar-block">
	<%@ include file="nav_editor_options.jsp" %>
</nav>

<nav id="editor-nav-sidebar" class="sidebar-block">
	<h2><s:message code="~eu.dariah.de.colreg.titles.properties" /></h2>
	
	<h3><a href="#mandatory_description"><s:message code="~eu.dariah.de.colreg.model.agent.groups.mandatory_description" /></a></h3>
	<ul>
		<s:bind path="agent.agentTypeId">
			<li ${status.error ? 'class="has-error"' : ' '}><a href="#agentTypeId"><s:message code="~eu.dariah.de.colreg.model.agent.type" /></a></li>
		</s:bind>
		<s:bind path="agent.name">
			<li class="agent-nonnatural-only ${status.error ? ' has-error' : ''}" <c:if test="${agentIsNatural}"> style="display: none;"</c:if>><a href="#name"><s:message code="~eu.dariah.de.colreg.model.agent.name" /></a></li>
			<li class="agent-natural-only ${status.error ? ' has-error' : ''}" <c:if test="${!agentIsNatural}"> style="display: none;"</c:if>><a href="#name"><s:message code="~eu.dariah.de.colreg.model.agent.last_name" /></a></li>
		</s:bind>
		<s:bind path="agent.foreName">
			<li class="agent-natural-only ${status.error ? ' has-error' : ''}" <c:if test="${!agentIsNatural}"> style="display: none;"</c:if>><a href="#foreName"><s:message code="~eu.dariah.de.colreg.model.agent.first_name" /></a></li>
		</s:bind>
	</ul>

		<h3><a href="#extended_description"><s:message code="~eu.dariah.de.colreg.model.agent.groups.extended_description" /></a></h3>
		<ul>
			<s:bind path="agent.addresses*">
				<li ${status.error ? 'class="has-error"' : ' '}><a href="#tbl-agent-addresses"><s:message code="~eu.dariah.de.colreg.model.agent.addresses" /></a></li>
			</s:bind>
			<s:bind path="agent.eMail">
				<li ${status.error ? 'class="has-error"' : ' '}><a href="#eMail"><s:message code="~eu.dariah.de.colreg.model.agent.email" /></a></li>
			</s:bind>
			<s:bind path="agent.webPage">
				<li ${status.error ? 'class="has-error"' : ' '}><a href="#webPage"><s:message code="~eu.dariah.de.colreg.model.agent.webpage" /></a></li>
			</s:bind>
			<s:bind path="agent.phone">
				<li ${status.error ? 'class="has-error"' : ' '}><a href="#phone"><s:message code="~eu.dariah.de.colreg.model.agent.phone" /></a></li>
			</s:bind>
		</ul>

		<h3><a href="#contextual"><s:message code="~eu.dariah.de.colreg.model.agent.groups.contextual" /></a></h3>
		<ul>
			<s:bind path="agent.providedIdentifier*">
				<li ${status.error ? 'class="has-error"' : ' '}><a href="#lst-agent-provided-identifiers"><s:message code="~eu.dariah.de.colreg.model.agent.provided_identifiers" /></a></li>
			</s:bind>
			<s:bind path="agent.parentAgentId">
				<li ${status.error ? 'class="has-error"' : ' '}><a href="#parentAgentIdSelector"><s:message code="~eu.dariah.de.colreg.model.agent.parent_agent" /></a></li>
			</s:bind>
			<li><a href="#child-agents"><s:message code="~eu.dariah.de.colreg.model.agent.child_agents" /></a></li>
			<li><a href="#associated-collections"><s:message code="~eu.dariah.de.colreg.model.agent.associated_collections" /></a></li>
		</ul>
		<c:if test="${!isNew}">
			<h3><a href="#identification_and_administration"><s:message code="~eu.dariah.de.colreg.model.agent.groups.identification_and_administration" /></a></h3>
			<ul>
				<li><a href="#agent-identifier"><s:message code="~eu.dariah.de.colreg.model.agent.agent_identifier" /></a></li>
				<li><a href="#version-identifier"><s:message code="~eu.dariah.de.colreg.model.agent.version_identifier" /></a></li>
				<li><a href="#current-version"><s:message code="~eu.dariah.de.colreg.model.agent.current_version" /></a></li>
				<li><a href="#initially-created"><s:message code="~eu.dariah.de.colreg.model.agent.created" /></a></li>
				<c:if test="${!isDeleted && editMode}">
					<li><a href="#agent-administration"><s:message code="~eu.dariah.de.colreg.model.agent.groups.administration" /></a></li>
				</c:if>
			</ul>
		</c:if>
</nav>