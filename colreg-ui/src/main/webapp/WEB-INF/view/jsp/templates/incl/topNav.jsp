<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tpl" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<tiles:importAttribute name="collapsePanel" ignore="true" />

<header>
	<nav class="navbar navbar-expand-xl bg-${_nuance} navbar-dark">
	
		<div class="container-fluid" style="max-width: 1200px">
	
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
	
				<span class="ti-menu"></span>
				<s:message code="~eu.dariah.de.colreg.common.menu" />
			</button>
		
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
				
					<li class="nav-item">
						<a class="nav-link" href="<s:url value='/' />"><span class="ti-home"></span></a>
					</li>
					<c:if test="${_draftCount > 0}">
						<li class="nav-item">
							<a class="nav-link" href="<s:url value='/drafts/' />"><s:message code="~eu.dariah.de.colreg.view.common.labels.drafts" arguments="${_draftCount}" /></a>
						</li>
					</c:if>
					<li class="nav-item">
						<a class="nav-link" href="<s:url value='/collections/' />"><s:message code="~eu.dariah.de.colreg.titles.collections" /></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<s:url value='/agents/' />"><s:message code="~eu.dariah.de.colreg.titles.agents" /></a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <s:message code="~eu.dariah.de.colreg.titles.vocabularies" /> <span class="ti-angle-down"></span></a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<div class="dropdown-menu-block block-with-icon">
								<span class="dropdown-header" href="#"><span class="nav-icon ti-list"></span> <s:message code="~eu.dariah.de.colreg.titles.vocabularies" /></span>
								<c:forEach items="${_vocabularies}" var="vocabulary">
									<a class="dropdown-item" href="<s:url value='/vocabularies/${vocabulary.id}/' />">${vocabulary.localizedLabel}</a>
								</c:forEach>
							</div>
						</div>
					</li>			
					<li class="nav-item dropdown">
						<a class="nav-link" href="#" id="helpDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <s:message code="~eu.dariah.de.colreg.view.help" /> <span class="ti-angle-down"></span></a>
						<div class="dropdown-menu" aria-labelledby="helpDropdown">
							<div class="dropdown-menu-block block-with-icon">
								<a class="dropdown-header" href="#"><i class="nav-icon fas fa-question-circle"></i> <s:message code="~eu.dariah.de.colreg.view.help" /> </a>
								
								<a href="https://de.dariah.eu/collection-registry" class="dropdown-item"><s:message code="~eu.dariah.de.colreg.view.help.portal" /></a>
								<a href="https://dfa.de.dariah.eu/doc/colreg/" class="dropdown-item"><s:message code="~eu.dariah.de.colreg.view.help.documents" /></a>
							</div>
							
							<div class="dropdown-menu-block block-with-icon">
								<span class="dropdown-header" href="#"><i class="nav-icon fab fa-gitlab"></i> <s:message code="~eu.dariah.de.colreg.view.help.source_code" /></span>
								<a href="https://gitlab.com/DARIAH-DE/colreg" class="dropdown-item"><s:message code="~eu.dariah.de.colreg.view.help.cr_gitlab" /></a>
								<a href="https://github.com/DARIAH-DE/DCDDM" class="dropdown-item"><s:message code="~eu.dariah.de.colreg.view.help.dcddm" /></a>
							</div>
						</div>
					</li>
				</ul>
			</div>
			
			<div class="navbar-expand" id="navbarSupportedContent2" style="margin-right: 40px;">
			
				<!-- Language, login, search -->
				<ul class="navbar-nav">
					<li class="nav-item dropdown navbar-separator">
						<a class="nav-link" href="#" id="languageDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="ti-world"> </span>
							<c:forEach items="${_LANGUAGES}" var="_LANGUAGE">
								<c:if test="${fn:startsWith(pageContext.response.locale, _LANGUAGE.key)}">
									${_LANGUAGE.value}
								</c:if>	
							</c:forEach>
						</a>
						<div class="dropdown-menu" aria-labelledby="languageDropdown">
							<c:forEach items="${_LANGUAGES}" var="_LANGUAGE">
								<c:if test="${fn:startsWith(pageContext.response.locale, _LANGUAGE.key)==false}">
									<a class="dropdown-item" href="?lang=${_LANGUAGE.key}">${_LANGUAGE.value}</a>
								</c:if>
							</c:forEach>
						</div>
					</li>
					<li class="nav-item">
						<c:set var="currentUrl" value="${requestScope['javax.servlet.forward.request_uri']}" />
						<c:choose>
							<c:when test="${_auth==null || _auth.auth!=true}">							
								<a class="nav-link account_toggle" href="<s:url value='/startLogin?url=${currentUrl}' />"><span class="ti-login"></span></a>
							</c:when>
							<c:otherwise>
								<a class="nav-link account_toggle" href="<s:url value='/centralLogout' />"><span class="ti-logout"></span></a>
							</c:otherwise>
						</c:choose>
					</li>
					<c:if test="${_auth!=null && _auth.auth==true}">
						<li class="nav-item">
							<a class="nav-link" href="<s:url value='/user' />" title="${_auth.displayName}"><span class="ti-user"></span></a>
						</li>
					</c:if>
					
					
					<c:catch>
						<tiles:importAttribute name="theme" />
						<jsp:include page="../../../../themes/${theme}/jsp/add_navigation.jsp" />
					</c:catch>

					<c:if test="${collapsePanel!=null}">
			        	<li class="nav-item navbar-separator grid-xl-hidden">
							<a class="nav-link version-panel-toggle collapse-panel-trigger" href="javascript:void"><i class="fas fa-archive fa-lg"></i></a>
						</li>
			        </c:if>
				</ul>
			</div>
			
		</div>
		<div class="container-fluid "style="position: absolute; top: 0; right: 0; width: 55px; padding: 0;">
			<div class="grid-hidden grid-xl-visible">
				<ul class="navbar-nav float-right">
					<c:if test="${collapsePanel!=null}">
			        	<li class="nav-item navbar-separator" style="width: 55px;">
							<a class="nav-link version-panel-toggle collapse-panel-trigger" href="javascript:void"><i class="fas fa-archive fa-lg"></i></a>
						</li>
			        </c:if>
			    </ul>
			</div>
		</div>
	</nav>
	<h1 class="logobar">
		<a class="logobar-link<c:if test="${smallLogo==true}"> logobar-link-sm</c:if>" href="" title="Startseite">
			<img class="logobar-logo" src='<s:url value="/theme/img/${smallLogo==true ? 'theme-logo-small.svg' : 'theme-logo-de.svg'}" />' alt='<s:message code="~eu.dariah.de.minfba.theme.name" />'>
			<span class="logobar-title">
				<s:message code="${smallLogo==true ? '~eu.dariah.de.minfba.theme.application_titles.cr' : '~eu.dariah.de.minfba.theme.application_titles.br.cr'}" />
			</span>
		</a>
	</h1>
</header>
<input id="currentUrl" type="hidden" value="${requestScope['javax.servlet.forward.request_uri']}" />
<input id="baseUrl" type="hidden" value="<s:url value="/" />" />
<input id="baseUrl2" type="hidden" value="<s:url value="/{}" />" />
