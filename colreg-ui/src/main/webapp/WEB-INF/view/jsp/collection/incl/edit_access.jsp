<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<s:bind path="accessMethods[${currIndex}].*">
	<tr class="list${status.error ? ' has-error' : ' '}">
		<td class="accessMethodTable_uri" onclick="editor.tables['accessMethodTable'].editEntry(this); return false;">
			<c:choose>
				<c:when test="${currMethod!=null}">
					${currMethod.uri}
				</c:when>
				<c:otherwise><s:message code="~eu.dariah.de.colreg.common.labels.new_entry" /></c:otherwise>
			</c:choose>
		</td>
		<td class="accessMethodTable_type nowrap" onclick="editor.tables['accessMethodTable'].editEntry(this); return false;">
			<c:if test="${currMethod!=null}">
				<c:forEach items="${accessTypes}" var="type">
					<c:if test="${currMethod.type==type.id}">${type.label}</c:if>
				</c:forEach>
			</c:if>
		</td>
		<c:if test="${editMode}">
			<td class="nowrap">
				<button onclick="editor.tables['accessMethodTable'].pushEntryUp(this); return false;" class="btn btn-xs btn-link btn-push-up"><i class="fas fa-long-arrow-alt-up"></i></button>
				<button onclick="editor.tables['accessMethodTable'].pushEntryDown(this); return false;" class="btn btn-xs btn-link btn-push-down"><i class="fas fa-long-arrow-alt-down"></i></button>
				<button onclick="editor.tables['accessMethodTable'].removeEntry(this); return false;" class="btn btn-xs btn-link"><i class="fas fa-times"></i></button>
			</td>
		</c:if>
	</tr>
</s:bind>
<tr class="edit" style="display: none;">
	<td colspan="${editMode ? 3 : 2}">
		<s:bind path="accessMethods[${currIndex}].type">
			<div class="form-group access-type row${status.error ? ' has-error' : ' '}">
				<label for="title" class="col-sm-3 col-form-label"><s:message code="~eu.dariah.de.colreg.model.access.type" /></label>
				
				<c:choose>
					<c:when test="${editMode}">
						<div class="col-sm-9">
							<span class="attribute-name-helper">accessMethods{}.type</span>
							<select class="form-control" name="accessMethods[${currIndex}].type" id="accessMethods${currIndex}.type" 
								onchange="editor.handleAccessTypeChange(this); editor.tables['accessMethodTable'].handleSelectChange(this, 'accessMethodTable_type');" autocomplete="off">
								<c:forEach items="${accessTypes}" var="type">
									<c:set var="selected"></c:set>
									<c:if test="${currMethod.type==type.id}"><c:set var="selected">selected="selected"</c:set></c:if>
									<option ${selected} value="${type.id}">${type.label}</option>
								</c:forEach>
							</select>
						</div>
						<div class="col-sm-9 offset-sm-3"><sf:errors element="div" cssClass="validation-error alert alert-danger" 
							path="accessMethods[${currIndex}].type" /></div>
					</c:when>
					<c:otherwise>
						<div class="col-sm-9">
							<label class="col-form-label col-content-label">
								<c:forEach items="${accessTypes}" var="type">
									<c:if test="${currMethod.type==type.id}">${type.label}</c:if>
								</c:forEach>
							</label>
						</div>
					</c:otherwise>
				</c:choose>		
				<div class="col-sm-9 offset-sm-3">
					<div class="editor-hint alert alert-info">
						<i class="far fa-question-circle"></i> 
						<s:message code="~eu.dariah.de.colreg.editorhint.access.type" />
					</div>
				</div>
			</div>
		</s:bind>
				
		<s:bind path="accessMethods[${currIndex}].uri">
			<div class="form-group access-uri row${status.error ? ' has-error' : ' '}">
				<label for="title" class="col-sm-3 col-form-label"><s:message code="~eu.dariah.de.colreg.model.access.uri" /> ${accessType.identifier}</label>
				<div class="col-sm-9">
					<c:choose>
						<c:when test="${editMode}">
							<span class="attribute-name-helper">accessMethods{}.uri</span>
							<input type="text" 
								onchange="editor.tables['accessMethodTable'].handleInputChange(this, 'accessMethodTable_uri');" 
								onkeyup="editor.tables['accessMethodTable'].handleInputChange(this, 'accessMethodTable_uri');" 
								class="form-control" id="accessMethods${currIndex}.uri" name="accessMethods[${currIndex}].uri" 
								value="<c:if test="${currMethod!=null}">${currMethod.uri}</c:if>">
						</c:when>
						<c:otherwise>
							<label class="col-form-label col-content-label">${currMethod!=null ? currMethod.uri : ''}</label>
						</c:otherwise>
					</c:choose>		
				</div>
				<div class="col-sm-9 offset-sm-3"><sf:errors element="div" cssClass="validation-error alert alert-danger" 
					path="accessMethods[${currIndex}].uri" /></div>
				<div class="col-sm-9 offset-sm-3">
					<div class="editor-hint alert alert-info">
						<i class="far fa-question-circle"></i> 
						<s:message code="~eu.dariah.de.colreg.editorhint.access.uri" />
					</div>
				</div>
			</div>
		</s:bind>
		
		<c:set var="isOaiPMH" value="false" />
		<c:set var="isREST" value="false" />
		<c:set var="isGit" value="false" />
		<c:set var="isFile" value="false" />
		<c:forEach items="${accessTypes}" var="accessType">
			<c:if test="${currMethod.type==accessType.id && accessType.identifier=='oaipmh'}">
				<c:set var="isOaiPMH" value="true" />
			</c:if>
			<c:if test="${currMethod.type==accessType.id && accessType.identifier=='onlinefile'}">
				<c:set var="isFile" value="true" />
			</c:if>
			<c:if test="${currMethod.type==accessType.id && accessType.identifier=='git'}">
				<c:set var="isGit" value="true" />
			</c:if>
			<c:if test="${currMethod.type==accessType.id && accessType.identifier=='rest'}">
				<c:set var="isREST" value="true" />
			</c:if>
		</c:forEach>
		
		<!-- Params -->
		<s:bind path="accessMethods[${currIndex}].params*">
			<div class="form-group access-params row${status.error ? ' has-error' : ''}" ${isFile || isGit || isOaiPMH || isREST ? '' : 'style="display: none;"'}>
				<label for="title" class="col-sm-3 col-form-label${status.error ? ' container-error' : ' '}"><s:message code="~eu.dariah.de.colreg.model.access.params" /></label>
				<div class="col-sm-9">
					<c:choose>
						<c:when test="${editMode}">
							<span class="attribute-name-helper">lst-collection-accessMethods-params{}</span>
							<ul class="editor-list editor-list-accessparams form-control-container" data-newrowurl="collections/includes/editAccessParam">
								<c:if test="${fn:length(currMethod.params)>0}">
									<c:forEach items="${currMethod.params}" var="parameter" varStatus="status" >
										<c:set var="currParam" value="${parameter}" scope="request" />
										<c:set var="paramIndex" value="${status.index}" scope="request" />
										<jsp:include page="edit_access_params.jsp" />
									</c:forEach>
									<c:remove var="currParam" />	
								</c:if>
								<c:if test="${editMode}">
									<li class="editor-list-buttons">
										<button class="btn btn-xs btn-link btn-add-list-element"><i class="fas fa-plus"></i> <s:message code="~eu.dariah.de.colreg.view.collection.actions.add_param" /></button>
									</li>
								</c:if>
							</ul>
						</c:when>
						<c:otherwise>
							<c:if test="${fn:length(currMethod.params)>0}">
								<label class="col-form-label col-content-label">
								<ul>
									<c:forEach items="${currMethod.params}" var="parameter">
										<li><b>${parameter.key}</b>:${parameter.value}</li> 
									</c:forEach>
								</ul>
								</label>
							</c:if>
						</c:otherwise>
					</c:choose>
				</div>
				<div class="col-sm-9 offset-sm-3"><sf:errors element="div" cssClass="validation-error alert alert-danger" 
					path="accessMethods[${currIndex}].params" /></div>
				<div class="col-sm-9 offset-sm-3">
					<div class="editor-hint alert alert-info">
						<i class="far fa-question-circle"></i> 
						<s:message code="~eu.dariah.de.colreg.editorhint.access.params" />
					</div>
				</div>
			</div>
		</s:bind>
		
		<!-- Header -->
		<s:bind path="accessMethods[${currIndex}].headers*">
			<div class="form-group access-headers row${status.error ? ' has-error' : ''}" ${isREST ? '' : 'style="display: none;"'}>
				<label for="title" class="col-sm-3 col-form-label${status.error ? ' container-error' : ' '}"><s:message code="~eu.dariah.de.colreg.model.access.headers" /></label>
				<div class="col-sm-9">
					<c:choose>
						<c:when test="${editMode}">
							<span class="attribute-name-helper">lst-collection-accessMethods-headers{}</span>
							<ul class="editor-list editor-list-accessheaders form-control-container" data-newrowurl="collections/includes/editAccessHeader">
								<c:if test="${fn:length(currMethod.headers)>0}">
									<c:forEach items="${currMethod.headers}" var="hdr" varStatus="status" >
										<c:set var="currHeader" value="${hdr}" scope="request" />
										<c:set var="headerIndex" value="${status.index}" scope="request" />
										<jsp:include page="edit_access_headers.jsp" />
									</c:forEach>
									<c:remove var="currHeader" />	
								</c:if>
								<c:if test="${editMode}">
									<li class="editor-list-buttons">
										<button class="btn btn-xs btn-link btn-add-list-element"><i class="fas fa-plus"></i> <s:message code="~eu.dariah.de.colreg.view.collection.actions.add_header" /></button>
									</li>
								</c:if>
							</ul>
						</c:when>
						<c:otherwise>
							<c:if test="${fn:length(currMethod.headers)>0}">
								<label class="col-form-label col-content-label">
								<ul>
									<c:forEach items="${currMethod.headers}" var="hdr">
										<li><b>${hdr.key}</b>:${hdr.value}</li> 
									</c:forEach>
								</ul>
								</label>
							</c:if>
						</c:otherwise>
					</c:choose>
				</div>
				<div class="col-sm-9 offset-sm-3"><sf:errors element="div" cssClass="validation-error alert alert-danger" 
					path="accessMethods[${currIndex}].headers" /></div>
				<div class="col-sm-9 offset-sm-3">
					<div class="editor-hint alert alert-info">
						<i class="far fa-question-circle"></i> 
						<s:message code="~eu.dariah.de.colreg.editorhint.access.headers" />
					</div>
				</div>
			</div>
		</s:bind>
		
		
		<s:bind path="accessMethods[${currIndex}].subtype">
			<div class="form-group access-subtype row subtype${status.error ? ' has-error' : ''}" ${isFile || isGit|| isREST ? '' : 'style="display: none;"'}>
				<label for="title" class="col-sm-3 col-form-label"><s:message code="~eu.dariah.de.colreg.model.access.subtype" />${accessMethods[currIndex].subtype}</label>
				<div class="col-sm-9">
					<c:choose>
						<c:when test="${editMode}">
							<label class="col-form-label col-content-label">
								<span class="attribute-name-helper">accessMethods{}.subtype</span> 
								<input ${collection.accessMethods[currIndex].subtype=="XML" ? 'checked="checked"' : ''} name="accessMethods[${currIndex}].subtype" value="XML" type="radio" />XML&nbsp;
							</label> 
							<label class="col-form-label col-content-label">
								<span class="attribute-name-helper">accessMethods{}.subtype</span> 
								<input ${collection.accessMethods[currIndex].subtype=="JSON" ? 'checked="checked"' : ''} name="accessMethods[${currIndex}].subtype" value="JSON" type="radio" />JSON&nbsp;
							</label> 
							<label class="col-form-label col-content-label">
								<span class="attribute-name-helper">accessMethods{}.subtype</span> 
								<input ${collection.accessMethods[currIndex].subtype=="CSV" ? 'checked="checked"' : ''} name="accessMethods[${currIndex}].subtype" value="CSV" type="radio" />CSV&nbsp;
							</label> 
							<label class="col-form-label col-content-label">
								<span class="attribute-name-helper">accessMethods{}.subtype</span> 
								<input ${collection.accessMethods[currIndex].subtype=="TSV" ? 'checked="checked"' : ''} name="accessMethods[${currIndex}].subtype" value="TSV" type="radio" />TSV&nbsp;
							</label> 
							<label class="col-form-label col-content-label">
								<span class="attribute-name-helper">accessMethods{}.subtype</span> 
								<input ${collection.accessMethods[currIndex].subtype=="Text" ? 'checked="checked"' : ''} name="accessMethods[${currIndex}].subtype" value="Text" type="radio" />Text&nbsp;
							</label>
						</c:when>
						<c:otherwise>
							<label class="col-form-label col-content-label">${currMethod!=null ? currMethod.subtype : ''}</label>
						</c:otherwise>
					</c:choose>	
				</div>
				<div class="col-sm-9 offset-sm-3"><sf:errors element="div" cssClass="validation-error alert alert-danger" 
					path="accessMethods[${currIndex}].subtype" /></div>
				<div class="col-sm-9 offset-sm-3">
					<div class="editor-hint alert alert-info">
						<i class="far fa-question-circle"></i> 
						<s:message code="~eu.dariah.de.colreg.editorhint.access.file_subtype" />
					</div>
				</div>
			</div>
		</s:bind>
		
		<!-- REST access method -->
		<s:bind path="accessMethods[${currIndex}].method">
			<div class="form-group access-method row subtype${status.error ? ' has-error' : ''}" ${isREST ? '' : 'style="display: none;"'}>
				<label for="title" class="col-sm-3 col-form-label"><s:message code="~eu.dariah.de.colreg.model.access.method" />${accessMethods[currIndex].method}</label>
				<div class="col-sm-9">
					<c:choose>
						<c:when test="${editMode}">
							<label class="col-form-label col-content-label">
								<span class="attribute-name-helper">accessMethods{}.method</span> 
								<input ${collection.accessMethods[currIndex].subtype=="GET" ? 'checked="checked"' : ''} name="accessMethods[${currIndex}].method" value="GET" type="radio" />GET&nbsp;
							</label> 
							<label class="col-form-label col-content-label">
								<span class="attribute-name-helper">accessMethods{}.method</span> 
								<input ${collection.accessMethods[currIndex].method=="POST" ? 'checked="checked"' : ''} name="accessMethods[${currIndex}].method" value="POST" type="radio" />POST&nbsp;
							</label> 
						</c:when>
						<c:otherwise>
							<label class="col-form-label col-content-label">${currMethod!=null ? currMethod.method : ''}</label>
						</c:otherwise>
					</c:choose>	
				</div>
				<div class="col-sm-9 offset-sm-3"><sf:errors element="div" cssClass="validation-error alert alert-danger" 
					path="accessMethods[${currIndex}].method" /></div>
				<div class="col-sm-9 offset-sm-3">
					<div class="editor-hint alert alert-info">
						<i class="far fa-question-circle"></i> 
						<s:message code="~eu.dariah.de.colreg.editorhint.access.file_subtype" />
					</div>
				</div>
			</div>
		</s:bind>
		
		
		<s:bind path="accessMethods[${currIndex}].patterns*">
			<div class="form-group access-patterns row patterns" ${isGit ? '' : 'style="display: none;"'}>
				<label for="title" class="col-sm-3 col-form-label${status.error ? ' container-error' : ' '}"><s:message code="~eu.dariah.de.colreg.model.access.access_patterns" /></label>
				<div class="col-sm-9">
					<c:choose>
						<c:when test="${editMode}">
							<span class="attribute-name-helper">lst-collection-accessMethods-patterns{}</span>
							<ul class="editor-list form-control-container" data-newrowurl="collections/includes/editPattern">
								<c:if test="${fn:length(currMethod.patterns)>0}">
									<c:forEach items="${currMethod.patterns}" var="pattern" varStatus="status" >
										<c:set var="currPattern" value="${pattern}" scope="request" />
										<c:set var="patternIndex" value="${status.index}" scope="request" />
										<jsp:include page="edit_pattern.jsp" />
									</c:forEach>
									<c:remove var="currPattern" />	
								</c:if>
								<c:if test="${editMode}">
									<li class="editor-list-buttons">
										<button class="btn btn-xs btn-link btn-add-list-element"><i class="fas fa-plus"></i> <s:message code="~eu.dariah.de.colreg.view.collection.actions.add_pattern" /></button>
									</li>
								</c:if>
							</ul>
						</c:when>
						<c:otherwise>
							<label class="col-form-label col-content-label">
								<c:forEach items="${currMethod.patterns}" var="pattern">
									${pattern}
									<br />
								</c:forEach>
							</label>
						</c:otherwise>
					</c:choose>
				</div>
				<div class="col-sm-9 offset-sm-3"><sf:errors element="div" cssClass="validation-error alert alert-danger" 
					path="accessMethods[${currIndex}].patterns" /></div>
				<div class="col-sm-9 offset-sm-3">
					<div class="editor-hint alert alert-info">
						<i class="far fa-question-circle"></i> 
						<s:message code="~eu.dariah.de.colreg.editorhint.access.patterns" />
					</div>
				</div>
			</div>
		</s:bind>
		
		<c:if test="${editMode}">
			<div class="form-group row test-access" ${isGit ? '' : 'style="display: none;"'}>
				<div class="col-sm-9 offset-sm-3">
					<button class="btn btn-primary float-right" onclick="editor.triggerTestAccess(this);"><i class="fas fa-cloud-download-alt"></i> <s:message code="~eu.dariah.de.colreg.common.actions.test_access" /></button>
				</div>
			</div>
		</c:if>
				
		
		<s:bind path="accessMethods[${currIndex}].accessModelId">
			<div class="form-group access-model-id row${status.error ? ' has-error' : ''}" ${isFile || isREST ? '' : 'style="display: none;"'}>
				<label for="title" class="col-sm-3 col-form-label"><s:message code="~eu.dariah.de.colreg.model.access.access_model" /></label>
				<div class="col-sm-9">
					<c:choose>
						<c:when test="${editMode}">
							<span class="attribute-name-helper">accessMethods{}.accessModelId</span>
							<select class="editor-list-input form-control" id="accessMethods[${currIndex}].accessModelId" name="accessMethods[${currIndex}].accessModelId" autocomplete="off">
								<option ${currMethod.accessModelId==null ? 'selected="selected" ' : ''}value=""><s:message code="~eu.dariah.de.colreg.common.labels.no_access_model" /></option>
								<option disabled>---------</option>
								<c:forEach items="${encodingSchemes}" var="scheme">
									<option ${currMethod.accessModelId==scheme.id ? 'selected="selected" ' : ''}value="${scheme.id}">${scheme.name} [${scheme.id}]</option>
								</c:forEach>
							</select>
						</c:when>
						<c:otherwise>
							<c:forEach items="${encodingSchemes}" var="scheme">
								<c:if test="${currMethod.accessModelId==scheme.id}">
									<label class="col-form-label col-content-label">${scheme.name} [${currMethod.accessModelId}]</label>
								</c:if>
							</c:forEach>
						</c:otherwise>
					</c:choose>		
				</div>
				<div class="col-sm-9 offset-sm-3"><sf:errors element="div" cssClass="validation-error alert alert-danger" 
					path="accessMethods[${currIndex}].accessModelId" /></div>
				<div class="col-sm-9 offset-sm-3">
					<div class="editor-hint alert alert-info">
						<i class="far fa-question-circle"></i> 
						<s:message code="~eu.dariah.de.colreg.editorhint.access.access_model" />
					</div>
				</div>
			</div>
		</s:bind>
		
		
		<s:bind path="accessMethods[${currIndex}].datamodels*">
			<div class="form-group access-datamodels row${status.error ? ' has-error' : ''}" ${isFile || isGit || isOaiPMH || isREST ? '' : 'style="display: none;"'}>
				<label for="title" class="col-sm-3 col-form-label${status.error ? ' container-error' : ' '}"><s:message code="~eu.dariah.de.colreg.model.access.encoding_schemes" /></label>
				<div class="col-sm-9">
					<c:choose>
						<c:when test="${editMode}">
							<span class="attribute-name-helper">lst-collection-accessMethods-datamodels{}</span>
							<ul class="editor-list form-control-container" data-newrowurl="collections/includes/editDatamodel">
								<c:if test="${fn:length(currMethod.datamodels)>0}">
									<c:forEach items="${currMethod.datamodels}" var="datamodel" varStatus="status" >
										<c:set var="currModel" value="${datamodel}" scope="request" />
										<c:set var="datamodelIndex" value="${status.index}" scope="request" />
										<jsp:include page="edit_datamodel.jsp" />
									</c:forEach>
									<c:remove var="currModel" />	
								</c:if>
								<c:if test="${editMode}">
									<li class="editor-list-buttons">
										<button class="btn btn-xs btn-link btn-add-list-element"><i class="fas fa-plus"></i> <s:message code="~eu.dariah.de.colreg.view.collection.actions.add_datamodel" /></button>
									</li>
								</c:if>
							</ul>
						</c:when>
						<c:otherwise>
							<label class="col-form-label col-content-label">
							<c:forEach items="${encodingSchemes}" var="scheme">
								<c:forEach items="${currMethod.datamodels}" var="datamodel">
									<c:if test="${datamodel.modelId==scheme.id}">${scheme.name}
										<c:if test="${datamodel.alias!=null}"> [${datamodel.alias}]</c:if>
										<br />
									</c:if>
								</c:forEach>
							</c:forEach>
							</label>
						</c:otherwise>
					</c:choose>
				</div>
				<div class="col-sm-9 offset-sm-3"><sf:errors element="div" cssClass="validation-error alert alert-danger" 
					path="accessMethods[${currIndex}].schemeIds" /></div>
				<div class="col-sm-9 offset-sm-3">
					<div class="editor-hint alert alert-info">
						<i class="far fa-question-circle"></i> 
						<s:message code="~eu.dariah.de.colreg.editorhint.access.scheme_ids" />
					</div>
				</div>
			</div>
		</s:bind>
		
		<s:bind path="accessMethods[${currIndex}].description">
			<div class="form-group row${status.error ? ' has-error' : ' '}">
				<label for="title" class="col-sm-3 col-form-label"><s:message code="~eu.dariah.de.colreg.model.access.description" /></label>
				<div class="col-sm-9">
					<c:choose>
						<c:when test="${editMode}">
							<span class="attribute-name-helper">accessMethods{}.description</span>
					<textarea class="form-control" rows="3" id="accessMethods${currIndex}.description" name="accessMethods[${currIndex}].description"><c:if test="${currMethod!=null}">${currMethod.description}</c:if></textarea>
						</c:when>
						<c:otherwise>
							<label class="col-form-label col-content-label">${currMethod!=null ? currMethod.description : ''}</label>
						</c:otherwise>
					</c:choose>
				</div>
				<div class="col-sm-9 offset-sm-3"><sf:errors element="div" cssClass="validation-error alert alert-danger" 
					path="accessMethods[${currIndex}].description" /></div>
				<div class="col-sm-9 offset-sm-3">
					<div class="editor-hint alert alert-info">
						<i class="far fa-question-circle"></i> 
						<s:message code="~eu.dariah.de.colreg.editorhint.access.description" />
					</div>
				</div>
			</div>
		</s:bind>
	</td>
</tr>