<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<s:bind path="relations[${currIndex}].${displayCollectionFieldname}">
	<div class="form-group row relation-collection-container${status.error ? ' has-error' : ''}">
		<label class="col-sm-3 col-form-label mandatory"><s:message code="~eu.dariah.de.colreg.model.collection_relation.related_collection" /></label>
		<div class="col-sm-9">
			<c:if test="${editMode}">
				<div class="row">
					<div class="col-12">
						<span class="attribute-name-helper">relations{}.${displayCollectionFieldname}</span>
						<sf:hidden cssClass="relation-collection-entityId" path="relations[${currIndex}].${displayCollectionFieldname}" />
						<input type="hidden" class="relation-collection-displayTitle" onchange="editor.tables['relationTable'].handleInputChange(this, 'relationTable_collection');"  />
						<input type="text" class="form-control relationCollectionEntityIdSelector" placeholder="<s:message code="~eu.dariah.de.colreg.view.collection.labels.search_by_id_name" />" />
					</div>
				</div>
			</c:if>
			<div class="row">
			
	
				<div class="col-md-12 mt-2">
					<div class="relatedCollection-display card card-primary-var-2 <c:if test="${relatedCollectionPojo==null}">hide</c:if>">
					  <div class="card-body">
					    <c:if test="${editMode}">
							<button type="button" class="btn btn-inline collection-reset float-right"><i class="fas fa-times fa-lg"></i></button>
						</c:if>
						<h3 class="card-title">
							<c:if test="${relatedCollectionPojo!=null}">   
								<a target="_blank" href="<s:url value="${relatedCollectionPojo.id}" />">
									${relatedCollectionPojo.displayTitle}
								</a>
						    </c:if>
					    </h3>
					    
					    <p class="card-text">ID: ${relatedCollectionPojo!=null ? relatedCollectionPojo.id : ''}</p>
					    
					  </div>
					</div>
					<div class="alert alert-sm alert-warning relatedCollection-draft-hint ${relatedCollectionPojo.draft ? '' : 'hide'}">
						<s:message code="~eu.dariah.de.colreg.editorhint.collection_relation.draft_relation" />
					</div>
					<div class="relatedCollection-display-null <c:if test="${relatedCollectionPojo!=null}">hide</c:if>">
						<label class="col-form-label col-content-label" style="text-align: left;">
							<em><s:message code="~eu.dariah.de.colreg.view.collection.labels.no_related_collection_set" /></em>
						</label>
					</div>
				</div>
			
				<div class="col-sm-9 offset-sm-3">
					<sf:errors element="div" cssClass="validation-error alert alert-danger" path="relations[${currIndex}]" />
				</div>
				<div class="col-sm-9 offset-sm-3">
					<div class="editor-hint alert alert-info">
						<i class="far fa-question-circle"></i> 
						<s:message code="~eu.dariah.de.colreg.editorhint.collection_relation.related_collection" />
					</div>
				</div>
			</div>			
		</div>
		<div class="col-sm-9 offset-sm-3"><sf:errors element="div" cssClass="validation-error alert alert-danger" path="relations[${currIndex}].${displayCollectionFieldname}" /></div>
		<div class="col-sm-9 offset-sm-3">
			<div class="editor-hint alert alert-info">
				<i class="far fa-question-circle"></i> 
				<s:message code="~eu.dariah.de.colreg.editorhint.collection.related_collections" />
			</div>
		</div>
	</div>
</s:bind>