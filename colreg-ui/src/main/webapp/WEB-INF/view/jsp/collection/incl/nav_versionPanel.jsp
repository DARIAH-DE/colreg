<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div id="editor-version-panel" class="version-panel">
	<div id="sidebar-wrapper" class="sidebar-wrapper">
		<div class="version-panel-header sticky-top">
			<h1><s:message code="~eu.dariah.de.colreg.titles.version_history" /></h1>
			<span class="close-sidebar pull-right"><i class="fas fa-lg fa-times"></i></span>
		</div>
		<c:forEach items="${versions}" var="version">
			<s:url value="/collections/${version.succeedingVersionId==null ? version.entityId : version.id}" var="agentLink" />
			
			<div class="card${version.id==selectedVersionId ? ' version-selected' : ''}${version.deleted ? ' version-deleted' : (version.draftUserId!=null ? ' version-draft' : ' version-published')}">
			  <div class="card-body">
			  
			  	<div class="card-heading">
			  		<c:choose>
						<c:when test="${version.deleted}">
							<div class="card-icon"><a href="${agentLink}"><i class="fas fa-lg fa-minus"></i></a></div>
						</c:when>
						<c:otherwise>
							<div class="card-icon"><a href="${agentLink}"><i class="fas fa-lg fa-check"></i></a></div>
						</c:otherwise>
					</c:choose>
			  		<h2 class="card-title">
			  			<a href="${agentLink}">
			  				<c:choose>
								<c:when test="${version.deleted}"> 
									<s:message code="~eu.dariah.de.colreg.common.labels.deleted" />
								</c:when>
								<c:when test="${version.draftUserId!=null}"> 
									<s:message code="~eu.dariah.de.colreg.common.labels.draft" />
								</c:when>
								<c:otherwise> 
									<s:message code="~eu.dariah.de.colreg.common.labels.published" />
								</c:otherwise>
							</c:choose>
			  			</a>
			  		</h2>
			  	</div>
			    <h3 class="card-subtitle"><joda:format value="${version.versionTimestamp}" style="MS" /></h3>
			    <h3 class="card-subtitle">${version.versionCreator}</h3>
		    	<c:if test="${version.versionComment!=null && version.versionComment!=''}">
					<p class="card-text">	
						"${version.versionComment}"
					</p>	
				</c:if>
			  </div>
			</div>
		</c:forEach>
	</div>
</div>