<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<s:bind path="params[${paramIndex}]">
	<li class="editor-list-item mb-2 form-item${status.error ? ' has-error' : ' '}">
		<div class="d-flex align-items-center">
			<div class="row">
				
				<div class="col-5">
					<span class="attribute-name-helper">params{}.key</span>
					<input type="text" class="editor-list-input editor-list-input-param-key form-control" id="params${paramIndex}.key" 
						name="params[${paramIndex}].key" value="${currParam.key}">
				</div>
				
				<div class="col-7">
					<span class="attribute-name-helper">params{}.value</span>
					<input type="text" class="editor-list-input editor-list-input-param-value form-control" id="params${paramIndex}.value" 
						name="params[${paramIndex}].value" value="${currParam.value}">
					
				</div>
			</div>
			<div class="editor-list-item-buttons ml-2">
				<button onclick="editor.lists['params'].pushEntryUp(this); return false;" class="btn btn-xs btn-link btn-push-up"><i class="fas fa-long-arrow-alt-up"></i></button>
				<button onclick="editor.lists['params'].pushEntryDown(this); return false;" class="btn btn-xs btn-link btn-push-down"><i class="fas fa-long-arrow-alt-down"></i></button>
				<button onclick="editor.lists['params'].removeEntry(this); return false;" class="btn btn-xs btn-link"><i class="fas fa-times"></i></button>
			</div>
		</div>
		<div class="col-sm-9 offset-sm-3"><sf:errors element="div" cssClass="validation-error alert alert-danger" path="params[${paramIndex}]" /></div>
	</li>
</s:bind>