<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<s:bind path="collectionImages[${currIndex}]">
	<li class="editor-list-item mb-2 form-item${status.error ? ' has-error' : ' '}">
		<div class="d-flex">
			<span class="attribute-name-helper">collectionImages{}</span>
			<input type="hidden" class="form-control editor-list-input" id="collectionImages${currIndex}" name="collectionImages[${currIndex}]" value="${currImage.id}">
			<span class="collection-image-container">
				<a <c:if test="${currImage.imageUrl==null}">style="display: none;"</c:if> id="collection-image-preview" href="${currImage.imageUrl}" class="venobox" data-gall="gall-collection" data-title="<s:message code="~eu.dariah.de.colreg.model.collection.collection_image_l" />">
					<img class="collection-image-thumb" src="${currImage.thumbnailUrl}" />
				</a>
				<img <c:if test="${currImage.imageUrl!=null}">style="display: none;"</c:if> id="collection-image-placeholder" src='<s:url value="/resources/img/page_icon_faded.png"></s:url>' />
			</span>
			<c:if test="${editMode}">
				<span class="editor-list-item-buttons">
					<button onclick="editor.lists['images'].pushEntryUp(this); return false;" class="btn btn-xs btn-link btn-push-up"><i class="fas fa-long-arrow-alt-up"></i></button>
					<button onclick="editor.lists['images'].pushEntryDown(this); return false;" class="btn btn-xs btn-link btn-push-down"><i class="fas fa-long-arrow-alt-down"></i></button>
					<button onclick="editor.lists['images'].removeEntry(this); return false;" class="btn btn-xs btn-link"><i class="fas fa-times"></i></button>
				</span>
			</c:if>
		</div>
		<div class="col-sm-9 offset-sm-3"><sf:errors element="div" cssClass="validation-error alert alert-danger" path="collectionImages[${currIndex}]" /></div>
	</li>
</s:bind>
