<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<s:bind path="accessMethods[${currIndex}].patterns[${patternIndex}]">
	<li class="editor-list-item mb-2 form-item${status.error ? ' has-error' : ' '}">
		<div class="d-flex">
			<span class="attribute-name-helper">accessMethods{}.patterns{}</span>
			<input type="text" class="editor-list-input form-control" id="accessMethods${currIndex}.patterns${patternIndex}" name="accessMethods[${currIndex}].patterns[${patternIndex}]" 
				value="<c:if test="${currPattern!=null}">${currPattern}</c:if>">
			<div class="editor-list-item-buttons">
				<button class="btn btn-xs btn-link btn-push-up"><i class="fas fa-long-arrow-alt-up"></i></button>
				<button class="btn btn-xs btn-link btn-push-down"><i class="fas fa-long-arrow-alt-down"></i></button>
				<button class="btn btn-xs btn-link btn-remove-entry"><i class="fas fa-times"></i></button>
			</div>
		</div>
		<div class="col-sm-9 offset-sm-3"><sf:errors element="div" cssClass="validation-error alert alert-danger" path="accessMethods[${currIndex}].patterns[${patternIndex}]" /></div>
	</li>
</s:bind>