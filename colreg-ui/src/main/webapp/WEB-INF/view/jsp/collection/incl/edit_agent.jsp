<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<s:bind path="agentRelations[${currIndex}].*">
	<tr class="list${status.error ? ' has-error' : ' '}">
		<td class="agentRelationTable_agentName" onclick="editor.tables['agentRelationTable'].editEntry(this); return false;">
			<c:choose>
				<c:when test="${currAgentRelation!=null}">
					${currAgentRelation.agent.name} ${currAgentRelation.agent.foreName}
				</c:when>
				<c:otherwise><s:message code="~eu.dariah.de.colreg.common.labels.new_entry" /></c:otherwise>
			</c:choose>
		</td>
		<td class="agentRelationTable_agentType" onclick="editor.tables['agentRelationTable'].editEntry(this); return false;">		
			<c:if test="${currAgentRelation!=null}">
				<c:forEach items="${agentRelationTypes}" var="type">
					<c:set var="contains" value="false" />
					<c:forEach items="${currAgentRelation.typeIds}" var="typeId">
						<c:if test="${typeId==type.identifier}">
							<c:set var="contains" value="true" />
						</c:if>
					</c:forEach>
					<c:if test="${contains}">${type.displayLabel} </c:if>
				</c:forEach>
			</c:if>
		</td>
		<c:if test="${editMode}">
			<td class="nowrap">
				<button onclick="editor.tables['agentRelationTable'].pushEntryUp(this); return false;" class="btn btn-xs btn-link btn-push-up"><i class="fas fa-long-arrow-alt-up"></i></button>
				<button onclick="editor.tables['agentRelationTable'].pushEntryDown(this); return false;" class="btn btn-xs btn-link btn-push-down"><i class="fas fa-long-arrow-alt-down"></i></button>
				<button onclick="editor.tables['agentRelationTable'].removeEntry(this); return false;" class="btn btn-xs btn-link"><i class="fas fa-times"></i></button>
			</td>
		</c:if>
	</tr>
</s:bind>
<tr class="edit" style="display: none;">
	<td colspan="${editMode ? 3 : 2}">
		<s:bind path="agentRelations[${currIndex}].typeIds">
			<div class="form-group row${status.error ? ' has-error' : ' '}">
				<label for="agentRelations${currIndex}.typeIds" class="col-sm-3 col-form-label linked-control-label mandatory">
					<a target="_blank" href="<s:url value='/vocabularies/${_agentRelationTypesVocabularyId}/' />">
						<i class="fa fa-link" aria-hidden="true"></i> <s:message code="~eu.dariah.de.colreg.model.agent_relation.relation" />
					</a>
				</label>
				<c:choose>
					<c:when test="${editMode}">
						<div class="col-sm-5">
							<span class="attribute-name-helper">agentRelations{}.typeIds</span>
							<select class="form-control select-relation-type" id="agentRelations${currIndex}.typeIds" name="agentRelations[${currIndex}].typeIds" size="4" multiple="multiple" autocomplete="off">
								<c:forEach items="${agentRelationTypes}" var="type">
									<c:set var="contains" value="false" />
									<c:forEach items="${currAgentRelation.typeIds}" var="typeId">
										<c:if test="${typeId==type.identifier}">
											<c:set var="contains" value="true" />
										</c:if>
									</c:forEach>
									<c:set var="selected"></c:set>
									<c:if test="${contains}"><c:set var="selected">selected="selected"</c:set></c:if>
									<option ${selected} value="${type.identifier}">${type.displayLabel}</option>
								</c:forEach>
							</select>
							<input type="hidden" class="agent-type-display-helper" onchange="editor.tables['agentRelationTable'].handleInputChange(this, 'agentRelationTable_agentType');" />
						</div>
					</c:when>
					<c:otherwise>
						<div class="col-sm-9">
							<label class="col-form-label col-content-label">
								<c:forEach items="${agentRelationTypes}" var="type">
									<c:forEach items="${currAgentRelation.typeIds}" var="typeId">
										<c:if test="${typeId==type.identifier}">${type.displayLabel}<br /></c:if>
									</c:forEach>
								</c:forEach>
							</label>
						</div>
					</c:otherwise>
				</c:choose>
				
				<div class="col-sm-9 offset-sm-3"><sf:errors element="div" cssClass="validation-error alert alert-danger" 
					path="agentRelations[${currIndex}].typeIds" /></div>
				<div class="col-sm-9 offset-sm-3">
					<div class="editor-hint alert alert-info">
						<i class="far fa-question-circle"></i> 
						<s:message code="~eu.dariah.de.colreg.editorhint.agent_relation.relation" />
					</div>
				</div>
			</div>			
		</s:bind>
		<s:bind path="agentRelations[${currIndex}].agentId">
			<div class="form-group row${status.error ? ' has-error' : ' '}">
				<label for="title" class="col-sm-3 col-form-label"><s:message code="~eu.dariah.de.colreg.model.agent_relation.agent" /></label>
				<div class="col-sm-9">
					<c:if test="${editMode}">
						<div class="row">
							<div class="col-sm-12">
								<span class="attribute-name-helper">agentRelations{}.agentId</span>
								<input type="hidden" class="form-control" id="agentRelations${currIndex}.agentId" name="agentRelations[${currIndex}].agentId" 
									value="<c:if test="${currAgentRelation!=null}">${currAgentRelation.agentId}</c:if>" />
								<input type="hidden" class="agent-name-display-helper" onchange="editor.tables['agentRelationTable'].handleInputChange(this, 'agentRelationTable_agentName');" />
								<input type="text" class="form-control typeahead agent-typeahead" placeholder="<s:message code="~eu.dariah.de.colreg.view.collection.labels.type_to_search" />" />
							</div>
						</div>
					</c:if>
					<div class="row">
						<div class="col-md-12 mt-2">
							<div class="agent-display card card-primary-var-2 <c:if test="${currAgentRelation.agent==null}">hide</c:if>">
							  <div class="card-body">
							    <c:if test="${editMode}">
									<button type="button" class="btn btn-inline agent-reset float-right"><i class="fas fa-times fa-lg"></i></button>
								</c:if>
							    <h3 class="card-title">
									<a target="_blank" href="<s:url value="/agents/${currAgentRelation.agent.entityId}" />">
										${currAgentRelation.agent.name} ${currAgentRelation.agent.foreName}
									</a>
								</h3>
							    <p class="card-text">ID: ${currAgentRelation.agent.entityId}</p>
							  </div>
							</div>
							<div class="agent-display-null <c:if test="${currAgentRelation.agent!=null}">hide</c:if>">
								<label class="col-form-label col-content-label"><em><s:message code="~eu.dariah.de.colreg.view.collection.labels.no_agent_set" /></em></label><br />
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-9 offset-sm-3">
					<sf:errors element="div" cssClass="validation-error alert alert-danger" path="agentRelations[${currIndex}].agentId" />
				</div>
				<div class="col-sm-9 offset-sm-3">
					<s:url value="/agents/new" var="newAgentUrl" />
					<div class="editor-hint alert alert-info">
						<i class="far fa-question-circle"></i> 
						<s:message code="~eu.dariah.de.colreg.editorhint.agent_relation.agent" arguments="${newAgentUrl}" />
					</div>
				</div>
				
			</div>
		</s:bind>
		<s:bind path="agentRelations[${currIndex}].annotation">
			<div class="form-group row${status.error ? ' has-error' : ' '}">
				<label for="description" class="col-sm-3 col-form-label"><s:message code="~eu.dariah.de.colreg.model.agent_relation.description" /></label>
				<div class="col-sm-9">
					<c:choose>
						<c:when test="${editMode}">
							<span class="attribute-name-helper">agentRelations{}.annotation</span>
							<textarea class="form-control" rows="3" id="agentRelations${currIndex}.annotation" name="agentRelations[${currIndex}].annotation"><c:if test="${currAgentRelation!=null}">${currAgentRelation.annotation}</c:if></textarea>
						</c:when>
						<c:otherwise>
							<label class="col-form-label col-content-label">${currAgentRelation!=null ? currAgentRelation.annotation : ''}</label>
						</c:otherwise>
					</c:choose>
				</div>
				<div class="col-sm-9 offset-sm-3"><sf:errors element="div" cssClass="validation-error alert alert-danger" 
					path="agentRelations[${currIndex}].annotation" /></div>
				<div class="col-sm-9 offset-sm-3">
					<div class="editor-hint alert alert-info">
						<i class="far fa-question-circle"></i> 
						<s:message code="~eu.dariah.de.colreg.editorhint.agent_relation.annotation" />
					</div>
				</div>
			</div>
		</s:bind>
	</td>
</tr>