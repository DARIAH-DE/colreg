<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<s:bind path="${vocabularyIdentifier}*">
	<div class="form-group row${status.error ? ' container-error' : ' '}">				
		<label for="lst-collection-${vocabularyIdentifier}" class="col-sm-3 col-form-label linked-control-label">
			<a target="_blank" href="<s:url value='/vocabularies/${vocabularyId}/' />"><i class="fa fa-link" aria-hidden="true"></i> <s:message code="${vocabularyMessageCode}" /></a>
		</label>
		<div id="lst-collection-${vocabularyIdentifier}-container" class="col-sm-9">
			<c:choose>
				<c:when test="${editMode}">
					<ul id="lst-collection-${vocabularyIdentifier}" class="editor-list">
						<c:choose>
							<c:when test="${fn:length(vocabularyModelItems)>0}">
								<c:forEach items="${vocabularyModelItems}" var="vocabularyModelItem" varStatus="status" >
									<c:set var="currIndex" value="${status.index}" scope="request" />
									<jsp:include page="edit_vocabulary_item.jsp" />
								</c:forEach>
								<c:remove var="currType" />	
							</c:when>
							<c:otherwise>
								<c:set var="currIndex" value="0" scope="request" />
								<jsp:include page="edit_vocabulary_item.jsp" />
							</c:otherwise>
						</c:choose>
						<c:if test="${editMode}">
							<li class="editor-list-buttons">
								<button onclick="editor.lists['${vocabularyIdentifier}'].triggerAddListElement(this);" class="btn btn-xs btn-link btn-collection-editor-add"><i class="fas fa-plus"></i> <s:message code="${vocabularyAddEntryCode}" /></button>
							</li>
						</c:if>
					</ul>
				</c:when>
				<c:otherwise>
					<label class="col-form-label col-content-label">
						<c:forEach items="${vocabularyModelItems}" var="vocabularyModelItem" varStatus="status" >
							<c:forEach items="${availableVocabularyItems}" var="availableVocabularyItem">
								<c:if test="${availableVocabularyItem.identifier==vocabularyModelItem}">
									${availableVocabularyItem.displayLabel}<br />
								</c:if>
							</c:forEach>
						</c:forEach>
					</label>
				</c:otherwise>
			</c:choose>
		</div>
		<div class="col-sm-9 offset-sm-3">
			<sf:errors element="div" cssClass="validation-error alert alert-danger" path="${vocabularyIdentifier}" />
		</div>
		<div class="col-sm-9 offset-sm-3">
			<div class="editor-hint alert alert-info">
				<i class="far fa-question-circle"></i> 
				<s:message code="${vocabularyHintCode}" />
			</div>
		</div>
	</div>
</s:bind>