<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<s:bind path="accessMethods[${currIndex}].datamodels[${datamodelIndex}]">
	<li class="editor-list-item mb-2 form-item${status.error ? ' has-error' : ' '}">
		<div class="d-flex align-items-center">
			<div class="row">
				<label class="col-12"><s:message code="~eu.dariah.de.colreg.model.access.encoding_schemes.model" /></label>
				
				<div class="col-12">
					<span class="attribute-name-helper">accessMethods{}.datamodels{}.modelId</span>
					<select class="editor-list-input form-control" id="accessMethods[${currIndex}].datamodels[${datamodelIndex}].modelId" name="accessMethods[${currIndex}].datamodels[${datamodelIndex}].modelId" autocomplete="off">
						<c:forEach items="${encodingSchemes}" var="scheme">
							<option ${currModel.modelId==scheme.id ? 'selected="selected" ' : ''}value="${scheme.id}">${scheme.name} [${scheme.id}]</option>
						</c:forEach>
					</select>
				</div>
				<label class="col-12 mt-3"><s:message code="~eu.dariah.de.colreg.model.access.encoding_schemes.alias" /></label>
				
				<div class="col-12">
					<span class="attribute-name-helper">accessMethods{}.datamodels{}.alias</span>
					<input type="text" class="editor-list-input form-control" id="accessMethods[${currIndex}].datamodels${datamodelIndex}.alias" 
						name="accessMethods[${currIndex}].datamodels[${datamodelIndex}].alias" value="<c:if test="${currModel.alias!=null}">${currModel.alias}</c:if>">
					
				</div>
			</div>
			<div class="editor-list-item-buttons ml-2">
				<button class="btn btn-xs btn-link btn-push-up"><i class="fas fa-long-arrow-alt-up"></i></button>
				<button class="btn btn-xs btn-link btn-push-down"><i class="fas fa-long-arrow-alt-down"></i></button>
				<button class="btn btn-xs btn-link btn-remove-entry"><i class="fas fa-times"></i></button>
			</div>
		</div>
		<div class="col-sm-9 offset-sm-3"><sf:errors element="div" cssClass="validation-error alert alert-danger" path="accessMethods[${currIndex}].datamodels[${datamodelIndex}]" /></div>
	</li>
</s:bind>