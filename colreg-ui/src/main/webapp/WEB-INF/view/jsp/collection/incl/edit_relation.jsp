<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<s:bind path="relations[${currIndex}]*">
	<tr class="list${status.error ? ' has-error' : ' '}">
		<td onclick="editor.tables['relationTable'].editEntry(this); return false;">
			<i class="fas fa-home" aria-hidden="true"></i>
		</td>
		
		<td class="nowrap" onclick="editor.tables['relationTable'].editEntry(this); return false;">
			<span class="relationTable_direction">
				<c:choose>
					<c:when test="${currRelation.bidirectional}">
						<i class="fas fa-arrows-alt-h"></i>
						<c:set var="currDirection" value="bidirectional" />
					</c:when>
					<c:when test="${currRelation.source.id==collection.entityId}">
						<i class="fas fa-long-arrow-alt-right"></i>
						<c:set var="currDirection" value="right" />
					</c:when>
					<c:otherwise>
						<i class="fas fa-long-arrow-alt-left"></i>
						<c:set var="currDirection" value="left" />
					</c:otherwise>
				</c:choose>
			</span>
			<span class="relationTable_relationType">
				<c:if test="${currRelation!=null}">
					<c:forEach items="${availableCollectionRelationTypes}" var="type">
						<c:if test="${currRelation.relationTypeId==type.identifier}">
							${type.displayLabel}
						</c:if>
					</c:forEach>
				</c:if>
			</span>
			
		
		</td>
		
		<td onclick="editor.tables['relationTable'].editEntry(this); return false;">
			<i class="far fa-dot-circle fa-color-${relatedCollectionPojo.draft ? 'warning' : 'info'}" aria-hidden="true"></i>
		</td>
			
		<td class="relationTable_collection explode" onclick="editor.tables['relationTable'].editEntry(this); return false;">
			${relatedCollectionPojo.displayTitle}
		</td>
		<c:if test="${editMode}">
			<td class="nowrap">
				<button onclick="editor.tables['relationTable'].pushEntryUp(this); return false;" class="btn btn-xs btn-link btn-push-up"><i class="fas fa-long-arrow-alt-up"></i></button>
				<button onclick="editor.tables['relationTable'].pushEntryDown(this); return false;" class="btn btn-xs btn-link btn-push-down"><i class="fas fa-long-arrow-alt-down"></i></button>
				<button onclick="editor.tables['relationTable'].removeEntry(this); return false;" class="btn btn-xs btn-link"><i class="fas fa-times"></i></button>
			</td>
		</c:if>
	</tr>
</s:bind>
<tr class="edit" style="display: none;">
	<td colspan="${editMode ? 5 : 4}">
	
		<s:bind path="relations[${currIndex}].relationTypeId">
		
			<span class="attribute-name-helper">relations{}.id</span>
			<sf:hidden cssClass="relation-direction-bidirectional" path="relations[${currIndex}].id"/>
		
			<div class="form-group row${status.error ? ' has-error' : ' '}">
				<label for="relationTypeId" class="col-sm-3 col-form-label linked-control-label mandatory">
					<a target="_blank" href="<s:url value='/vocabularies/${_collectionRelationTypesVocabularyId}/' />">
						<i class="fa fa-link" aria-hidden="true"></i> <s:message code="~eu.dariah.de.colreg.model.collection_relation.relation_type" />
					</a>
				</label>				
				<c:choose>
					<c:when test="${editMode}">
						<div class="col-9">
							<span class="attribute-name-helper">relations{}.relationTypeId</span>
							<sf:select cssClass="form-control" path="relations[${currIndex}].relationTypeId" items="${availableCollectionRelationTypes}" itemLabel="displayLabel" itemValue="identifier"
								id="relations${currIndex}.relationTypeId" name="relations[${currIndex}].relationTypeId" 
								onchange="editor.tables['relationTable'].handleInputChange(this, 'relationTable_relationType', $(this).find('option:selected').text());" 
								onkeyup="editor.tables['relationTable'].handleInputChange(this, 'relationTable_relationType', $(this).find('option:selected').text());" />
						</div>
					</c:when>
					<c:otherwise>
						<div class="col-9">
							<label class="col-form-label col-content-label">
								<c:forEach items="${availableCollectionRelationTypes}" var="type">
									<c:if test="${currRelation.relationTypeId==type.identifier}">
										${type.displayLabel}
									</c:if>
								</c:forEach>
							</label>
						</div>
					</c:otherwise>
				</c:choose>
				
				<div class="col-sm-9 offset-sm-3"><sf:errors element="div" cssClass="validation-error alert alert-danger" path="relations[${currIndex}].relationTypeId" /></div>
				<div class="col-sm-9 offset-sm-3">
					<div class="editor-hint alert alert-info">
						<i class="far fa-question-circle"></i> 
						<s:message code="~eu.dariah.de.colreg.editorhint.collection_relation.relation_type" />
					</div>
				</div>
			</div>
		</s:bind>
	
		<s:bind path="relations[${currIndex}].bidirectional">
			<div class="form-group row${status.error ? ' has-error' : ' '}">
				<label for="relationTypeId" class="col-sm-3 col-form-label mandatory"><s:message code="~eu.dariah.de.colreg.model.collection_relation.direction" /></label>
				
				<span class="attribute-name-helper">relations{}.bidirectional</span>
				<sf:hidden cssClass="relation-direction-bidirectional" path="relations[${currIndex}].bidirectional"/>
				
				<c:choose>
					<c:when test="${editMode}">
						<div class="col-9">
							<div class="radio">
							  <label>
							  	<span class="attribute-name-helper">relationsHelper{}.direction</span>
							    <input type="radio"
							    	onchange="editor.handleRelationDirectionRadioChange(this);" 
							    	name="relationsHelper[${currIndex}].direction" id="relationsHelper${currIndex}.direction" 
							    	value="right" ${currDirection=="right" ? " checked" : ""}>
							    <i class="fas fa-home" aria-hidden="true"></i> 
							    <i class="fas fa-long-arrow-alt-right" aria-hidden="true"></i> 
							    <i class="far fa-dot-circle" aria-hidden="true"></i>
							  </label>
							</div>
							<div class="radio">
							  <label>
							    <span class="attribute-name-helper">relationsHelper{}.direction</span>
							    <input type="radio" 
							    	onchange="editor.handleRelationDirectionRadioChange(this);" 
							    	name="relationsHelper[${currIndex}].direction" id="relationsHelper${currIndex}.direction" 
							    	value="left"${currDirection=="left" ? " checked" : ""}>
							    <i class="fas fa-home" aria-hidden="true"></i> 
							    <i class="fas fa-long-arrow-alt-left" aria-hidden="true"></i> 
							    <i class="far fa-dot-circle" aria-hidden="true"></i>
							  </label>
							</div>
							<div class="radio">
							  <label>
							  	<span class="attribute-name-helper">relationsHelper{}.direction</span>
							    <input type="radio" 
							   		onchange="editor.handleRelationDirectionRadioChange(this);" 
							    	name="relationsHelper[${currIndex}].direction" id="relationsHelper${currIndex}.direction"  
							    	value="bidirectional"${currDirection=="bidirectional" ? " checked" : ""}>
							    <i class="fas fa-home" aria-hidden="true"></i> 
							    <i class="fas fa-arrows-alt-h" aria-hidden="true"></i> 
							    <i class="far fa-dot-circle" aria-hidden="true"></i>
							  </label>
							</div>
						
							<span class="attribute-name-helper">relations{}.bidirectional</span>
							<input type="hidden" class="agent-type-display-helper" onchange="editor.tables['relationTable'].handleInputChange(this, 'relationTable_bidirectional');" />
						</div>
					</c:when>
					<c:otherwise>
						<div class="col-sm-9">
							<label class="col-form-label col-content-label">
							
								<c:choose>
									<c:when test="${currDirection=='right'}">
										<i class="fas fa-home" aria-hidden="true"></i> 
								   	 	<i class="fas fa-long-arrow-alt-right" aria-hidden="true"></i> 
								    	<i class="far fa-dot-circle" aria-hidden="true"></i>
									</c:when>
									<c:when test="${currDirection=='left'}">
										<i class="fas fa-home" aria-hidden="true"></i> 
									    <i class="fas fa-long-arrow-alt-left" aria-hidden="true"></i> 
									    <i class="far fa-dot-circle" aria-hidden="true"></i>
									</c:when>
									<c:otherwise>
										<i class="fas fa-home" aria-hidden="true"></i> 
									    <i class="fas fa-arrows-alt-h" aria-hidden="true"></i> 
									    <i class="far fa-dot-circle" aria-hidden="true"></i>
									</c:otherwise>
								</c:choose>
							</label>
						</div>
					</c:otherwise>
				</c:choose>
				
				<div class="col-sm-9 offset-sm-3"><sf:errors element="div" cssClass="validation-error alert alert-danger" path="relations[${currIndex}].bidirectional" /></div>
				<div class="col-sm-9 offset-sm-3">
					<div class="editor-hint alert alert-info">
						<i class="far fa-question-circle"></i> 
						<s:message code="~eu.dariah.de.colreg.editorhint.collection_relation.direction" />
					</div>
				</div>
			</div>
		</s:bind>
	
		<!-- Related collection block -->
		<jsp:include page="edit_relation_collection.jsp" />
		
		<s:bind path="relations[${currIndex}].description">
			<div class="form-group row${status.error ? ' has-error' : ' '}">
				<label for="description" class="col-sm-3 col-form-label"><s:message code="~eu.dariah.de.colreg.model.collection_relation.description" /></label>
				<div class="col-sm-9">
					<c:choose>
						<c:when test="${editMode}">
							<span class="attribute-name-helper">relations{}.description</span>
							<textarea class="form-control" rows="3" id="relations${currIndex}.description" name="relations[${currIndex}].description">${currRelation.description}</textarea>
						</c:when>
						<c:otherwise>
							<label class="col-form-label col-content-label">${currRelation.description}</label>
						</c:otherwise>
					</c:choose>
				</div>
				<div class="col-sm-9 offset-sm-3">
					<div class="editor-hint alert alert-info">
						<i class="far fa-question-circle"></i> 
						<s:message code="~eu.dariah.de.colreg.editorhint.collection_relation.description" />
					</div>
				</div>
			</div>
		</s:bind>
			
	</td>
</tr>


