<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<s:url value="/agents/${version.succeedingVersionId==null ? version.entityId : version.id}" var="agentLink" />

<div class="card${version.deleted ? ' version-deleted ' : ' version-published'}">
  <div class="card-body">
  
  	<div class="card-heading">
  		<c:choose>
			<c:when test="${not empty version.foreName}">
				<div class="card-icon"><a href="${agentLink}"><i class="fas fa-lg fa-user"></i></a></div>
			</c:when>
			<c:otherwise>
				<div class="card-icon"><a href="${agentLink}"><i class="fas fa-lg fa-university"></i></a></div>
			</c:otherwise>
		</c:choose>
  		<h2 class="card-title"><a href="${agentLink}">${version.name}<c:if test="${not empty version.foreName}">, ${version.foreName}</c:if></a></h2>
  	</div>
  
    
    <h3 class="card-subtitle"><joda:format value="${version.versionTimestamp}" style="MS" /></h3>
    <h3 class="card-subtitle">${version.versionCreator}</h3>
    <p class="card-text">
		<c:choose>
			<c:when test="${version.deleted}">
				<i class="far fa-minus-square"></i> <s:message code="~eu.dariah.de.colreg.common.labels.agent_deleted" /> 
			</c:when>
			<c:otherwise>
				<i class="far fa-check-square"></i> <s:message code="~eu.dariah.de.colreg.common.labels.agent_published" />
			</c:otherwise>
		</c:choose>
		<c:if test="${version.versionComment!=null && version.versionComment!=''}">: "${version.versionComment}"</c:if>
	</p>
  </div>
</div>