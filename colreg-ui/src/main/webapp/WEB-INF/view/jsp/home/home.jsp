<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<tiles:importAttribute name="fluidLayout" />

<div class="container<c:if test="${fluidLayout==true}">-fluid</c:if>">
	<div class="row">
    <div class="col-md-6 col-lg-7 col-xl-8">
    

    	<div class="row">
	    	<div class="col-12">
	    		<h1><s:message code="~eu.dariah.de.colreg.titles.collection_registry" /></h1>
	    	</div>	    	
    	</div>
    
       	<div class="row">
	    	<div class="col-12">
	    		<ul>
	    			<li><a href="<s:url value="/collections/" />">${collectionCount} <s:message code="~eu.dariah.de.colreg.common.labels.link_to_collections" /></a></li>  
					<li><a href="<s:url value="/agents/" />">${agentCount} <s:message code="~eu.dariah.de.colreg.common.labels.link_to_agents" /></a></li>
	    		</ul>
	    	</div>
	    	<!-- <div class="col-md-6">
	    		<button class="float-md-right" id="btn-download-svg"><i class="fa fa-download" aria-hidden="true"></i> <s:message code="~eu.dariah.de.colreg.view.graph.download_svg" /></button>
	    	</div> -->
   		</div>
        	
       	<div class="row">
   			<div class="col-12">
	    	
	    		<div id="graph-container"></div>
	    
	    	</div> 
	    	
	    	
   		</div>
        	

				
        	

    
    
    </div>
    <div class="col-md-6 col-lg-5 col-xl-4">
     
     	<h1><s:message code="~eu.dariah.de.colreg.common.labels.latest_activities" /></h1>
		<div id="dashboard-version-panel" class="version-panel row">
			
			<c:forEach items="${latest}" var="version">
			
				<c:set var="version" value="${version}" scope="request" />
				<c:set var="isAgent" value="false" />
				<c:catch var="exception"><c:set var="isAgent" value="${version.collectionTypes!=null}" /></c:catch>
			
				<c:choose>
						<c:when test="${isAgent}">
							<jsp:include page="incl/latest_collection.jsp" />
						</c:when>
						<c:otherwise>
							<jsp:include page="incl/latest_agent.jsp" />
						</c:otherwise>
					</c:choose>
			
			</c:forEach>

		
		
			<!-- <ul>
				<c:forEach items="${latest}" var="version">
					<c:set var="version" value="${version}" scope="request" />
					<c:set var="isAgent" value="false" />
					<c:catch var="exception"><c:set var="isAgent" value="${version.collectionTypes!=null}" /></c:catch>
					<c:choose>
						<c:when test="${isAgent}">
							<jsp:include page="incl/latest_collection.jsp" />
						</c:when>
						<c:otherwise>
							<jsp:include page="incl/latest_agent.jsp" />
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</ul>	 -->					
		</div>
     
    </div>
  </div>
</div>