<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<s:url value="/collections/${version.succeedingVersionId==null ? version.entityId : version.id}" var="collectionLink" />

<div class="card${version.deleted ? ' version-deleted ' : (version.draftUserId!=null ? ' version-draft' : ' version-published')}">
  <div class="card-body">
  
  	<div class="card-heading">
		<div class="card-icon"><a href="${collectionLink}"><i class="fas fa-lg fa-file-contract"></i></a></div>
  		<h2 class="card-title"><a href="${collectionLink}">${version.localizedDescriptions[0].title}</a></h2>
  	</div>
  
    <h3 class="card-subtitle"><joda:format value="${version.versionTimestamp}" style="MS" /></h3>
    <h3 class="card-subtitle">${version.versionCreator}</h3>
    <p class="card-text">
		<c:choose>
			<c:when test="${version.deleted}">
				<i class="far fa-minus-square"></i> <s:message code="~eu.dariah.de.colreg.common.labels.collection_deleted" />
			</c:when>
			<c:when test="${version.draftUserId!=null}">
				<i class="far fa-check-square"></i> <s:message code="~eu.dariah.de.colreg.common.labels.draft_saved" />
			</c:when>
			<c:otherwise>
				<i class="far fa-check-square"></i> <s:message code="~eu.dariah.de.colreg.common.labels.collection_published" />
			</c:otherwise>
		</c:choose>
		<c:if test="${version.versionComment!=null && version.versionComment!=''}">: "${version.versionComment}"</c:if>
	</p>
  </div>
</div>