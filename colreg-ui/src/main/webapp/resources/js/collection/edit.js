var editor;
$(document).ready(function() {
	editor = new CollectionEditor();

	$("#btn-delete-collection").on("click", function() {
		editor.deleteEntity("collections/");
	});
	
	$("#btn-publish-collection").on("click", function() {
		$("form").attr("action", $("#js-form-action").val() + "/publish");
		$("form").submit();
	});
		
	$('[data-toggle="tooltip"]').tooltip();
});

var CollectionEditor = function() {
	var _this = this;
	this.collectionId = $("#entityId").text();
	this.imageMaxFileSize = 5242880;
	
	this.prepareTranslations([
		"~eu.dariah.de.colreg.view.collection.labels.add_uom",
		"~eu.dariah.de.colreg.common.labels.no_image",
		"~eu.dariah.de.colreg.view.collection.labels.image_not_an_image",
		"~eu.dariah.de.colreg.view.collection.labels.image_too_large",
		"~eu.dariah.de.colreg.common.labels.draft",
        "~eu.dariah.de.colreg.common.labels.published",
        "~eu.dariah.de.colreg.model.access.oaipmh_set",
        "~eu.dariah.de.colreg.model.access.git_branch",
        
        "~eu.dariah.de.colreg.view.access.test.accessibility",
        "~eu.dariah.de.colreg.view.access.test.clone_or_pull",
        "~eu.dariah.de.colreg.view.access.test.apply_pattern",
        "~eu.dariah.de.colreg.view.access.test.resulting_files",
        "~eu.dariah.de.colreg.view.access.test.errors.no_files"
	]);
	this.initVocabularySources();
	this.initEditorComponents();
	
	this.registerLanguageTypeahead($(".language-typeahead"));
	this.registerEncodingSchemeTypeahead($(".encoding-scheme-typeahead"));
	this.registerAgentTypeahead($(".agent-typeahead"));
	this.registerAgentRelationTypeSelection($(".select-relation-type"));
	
	this.initVersionSlidebar();
	this.registerNavFormControlEvents();
	this.registerFormControlSelectionEvents($("form"));
	this.initRightsContainer();
	
	$(".relationCollectionEntityIdSelector").each(function() {
		_this.registerRelatedCollectionTypeahead($(this));
	});
};

CollectionEditor.prototype = new BaseEditor();

CollectionEditor.prototype.initVocabularySources = function() {
	this.addVocabularySource("languages", "languages/query/");
	this.addVocabularySource("agents", "agents/query/");
	this.addVocabularySource("schemes", "schemes/query/");	
	this.addVocabularySource("relatableCollections", "collections/query/", "excl=" + this.entityId);
};

CollectionEditor.prototype.initEditorComponents = function() {
	var _this = this;
	
	// Editor tables
	this.tables["descriptionTable"] = new CollectionEditorTable({
		tableSelector: "#tbl-collection-description-sets",
		newRowUrl: __util.composeUrl("collections/includes/editDescription"),
		newRowCallback: function(row) {
			_this.registerLanguageTypeahead($(row).find(".language-typeahead"));
			_this.registerFormControlSelectionEvents($(row));
			$("#chk-toggle-hints").trigger("change");
		}
	});
	this.tables["agentRelationTable"] = new CollectionEditorTable({
		tableSelector: "#tbl-collection-agents",
		newRowUrl: __util.composeUrl("collections/includes/editAgent"),
		newRowCallback: function(row) {
			_this.registerAgentTypeahead($(row).find(".agent-typeahead"));
			_this.registerAgentRelationTypeSelection($(row).find(".select-relation-type"));
			_this.registerFormControlSelectionEvents($(row));
			$("#chk-toggle-hints").trigger("change");
		}
	});
	this.tables["accessMethodTable"] = new CollectionEditorTable({
		tableSelector: "#tbl-collection-access",
		newRowUrl: __util.composeUrl("collections/includes/editAccess"),
		newRowCallback: function(row, table) {
			_this.registerEncodingSchemeTypeahead($(row).find(".encoding-scheme-typeahead"));
			_this.registerFormControlSelectionEvents($(row));
			$("#chk-toggle-hints").trigger("change");

			_this.initDynamicEditorList(table, row);
		},
		initCallback: function(table) {
			_this.initDynamicEditorList(table, table.table);
		}
	});
	this.tables["accrualMethodTable"] = new CollectionEditorTable({
		tableSelector: "#tbl-collection-accrual",
		newRowUrl: __util.composeUrl("collections/includes/editAccrual"),
		newRowCallback: function(row) {
			_this.registerFormControlSelectionEvents($(row));
			$("#chk-toggle-hints").trigger("change");
		}
	});
	this.tables["locations"] = new CollectionEditorTable({
		tableSelector: "#tbl-collection-locations",
		newRowUrl: __util.composeUrl("collections/includes/editLocation"),
		newRowCallback: function(row) {
			_this.registerFormControlSelectionEvents($(row));
			$("#chk-toggle-hints").trigger("change");
		}
	});
	this.tables["relationTable"] = new CollectionEditorTable({
		tableSelector: "#tbl-collection-relations",
		newRowUrl: __util.composeUrl("collections/includes/editRelation"),
		newRowCallback: function(row) {
			_this.registerFormControlSelectionEvents($(row));
			_this.registerRelatedCollectionTypeahead($(row).find(".relationCollectionEntityIdSelector"));
			$("#chk-toggle-hints").trigger("change");
		}
	});
	
	
	// Editor lists
	this.lists["itemLanguageList"] = new CollectionEditorList({
		listSelector: "#lst-collection-item-languages",
		newRowUrl: __util.composeUrl("collections/includes/editItemLanguage"),
		newRowCallback: function(row) {
			_this.registerLanguageTypeahead($(row).find(".language-typeahead"));
			_this.registerFormControlSelectionEvents($(row));
		}
	});
	this.lists["identifierList"] = new CollectionEditorList({
		listSelector: "#lst-collection-provided-identifiers",
		newRowUrl: __util.composeUrl("collections/includes/editProvidedIdentifier"),
		newRowCallback: function(row) {
			_this.registerFormControlSelectionEvents($(row));
		}
	});
	this.lists["spatials"] = new CollectionEditorList({
		listSelector: "#lst-collection-spatials",
		newRowUrl: __util.composeUrl("collections/includes/editSpatial"),
		newRowCallback: function(row) {
			_this.registerFormControlSelectionEvents($(row));
		}
	});
	this.lists["temporals"] = new CollectionEditorList({
		listSelector: "#lst-collection-temporals",
		newRowUrl: __util.composeUrl("collections/includes/editTemporal"),
		newRowCallback: function(row) {
			_this.registerFormControlSelectionEvents($(row));
		}
	});
	this.lists["subjects"] = new CollectionEditorList({
		listSelector: "#lst-collection-subjects",
		newRowUrl: __util.composeUrl("collections/includes/editSubject"),
		newRowCallback: function(row) {
			_this.registerFormControlSelectionEvents($(row));
		}
	});
	this.lists["audiences"] = new CollectionEditorList({
		listSelector: "#lst-collection-audiences",
		newRowUrl: __util.composeUrl("collections/includes/editAudience"),
		newRowCallback: function(row) {
			_this.registerFormControlSelectionEvents($(row));
		}
	});
	this.lists["images"] = new CollectionEditorList({
		listSelector: "#lst-collection-images",
		newRowUrl: __util.composeUrl("collections/includes/editImage"),
		newRowCallback: function(row) {
			_this.registerFormControlSelectionEvents($(row));
			$(".venobox").venobox();
		}
	});
	
	
	this.lists["params"] = new CollectionEditorList({
		listSelector: "#lst-params-container",
		newRowUrl: __util.composeUrl("collections/includes/editParam"),
		newRowCallback: function(row) {
			_this.registerFormControlSelectionEvents($(row));
		}
	});
	
	this.lists["collectionTypes"] = new CollectionEditorList({
		listSelector: "#lst-collection-collectionTypes",
		newRowUrl: __util.composeUrl("collections/includes/editCollectionType"),
		newRowCallback: function(row) {
			_this.registerFormControlSelectionEvents($(row));
		}
	});
	this.lists["itemTypes"] = new CollectionEditorList({
		listSelector: "#lst-collection-itemTypes",
		newRowUrl: __util.composeUrl("collections/includes/editItemType"),
		newRowCallback: function(row) {
			_this.registerFormControlSelectionEvents($(row));
		}
	});
};

CollectionEditor.prototype.initDynamicEditorList = function(owner, container) {
	var _this = this;
	
	$(container).find(".editor-list").each(function() {
		var list = this;
		var newRowUrl = $(this).data("newrowurl");
		
		var l = new CollectionEditorList({
			listSelector: function() { return "#" + $(list).prop("id"); },
			owner: owner,
			newRowUrl: __util.composeUrl(newRowUrl),
			newRowCallback: function(row) {
				_this.registerFormControlSelectionEvents($(row));
				_this.initDynamicEditorListButtons(row, l);
			}
		});
		
		$(this).find(".btn-add-list-element").bind("click", function(event, data) {
			l.triggerAddListElement(this, data); 
		});
		_this.initDynamicEditorListButtons(this, l);
	});
};

CollectionEditor.prototype.initDynamicEditorListButtons = function(container, list) {
	$(container).find(".btn-push-up").bind("click", function() { list.pushEntryUp(this); });
	$(container).find(".btn-push-down").bind("click", function() { list.pushEntryDown(this); });
	$(container).find(".btn-remove-entry").bind("click", function() { list.removeEntry(this); });
};


CollectionEditor.prototype.registerLanguageTypeahead = function(element) {
	var _this = this;
	this.registerTypeahead(element, "languages", "code", 8, 
			function(data) { return '<p><strong>' + data.code + '</strong> – ' + data.name + '</p>'; },
			function(t, suggestion) { $(t).closest(".form-group").removeClass("has-error"); },
			function(t, value) { _this.validateInput(t, "languages/", value); }
	);
};

CollectionEditor.prototype.registerAgentTypeahead = function(element) {
	var _this = this;
	this.registerTypeahead(element, "agents", "none", 6, 
			function(data) { return "<p>" + _this.renderAgentSuggestion(data) + "</p>"; },
			function(t, suggestion) { _this.handleAgentSelection(true, t, suggestion); }
	);
	element.closest(".form-group").find(".agent-reset").on("click", function() { 
		_this.handleAgentSelection(false, this, null); 
	});
};

CollectionEditor.prototype.registerEncodingSchemeTypeahead = function(element) {
	var _this = this;
	this.registerTypeahead(element, "schemes", "name", 8, 
			function(data) { return "<p><strong>" + data.name + "</strong><br />" + data.url + "</p>"; },
			function(t, suggestion) { $(t).closest(".form-group").removeClass("has-error"); },
			function(t, value) { _this.validateInput(t, "schemes/", value); }
	);
};

CollectionEditor.prototype.registerRelatedCollectionTypeahead = function(element) {
	var _this = this;
	
	this.registerTypeahead(element, "relatableCollections", "none", 8, 
			function(data) { return "<p>" + _this.renderCollectionSuggestion(data) + "</p>"; },
			function(t, suggestion) {
				_this.handleRelatedCollectionSelection(element, true, suggestion.entityId, suggestion.localizedDescriptions[0].title);
			}, null
	);
	element.closest("td").find(".collection-reset").on("click", function() { 
		_this.handleRelatedCollectionSelection(element, false, "", ""); 
	});
};

CollectionEditor.prototype.handleRelatedCollectionSelection = function(element, select, entityId, title) {
	
	var container = $(element).closest(".form-group");
	$(container).find(".relation-collection-entityId").val(entityId);
		
	container = $(element).closest("td");
	$(container).find(".relation-collection-entityId").val(entityId);
	$(container).find(".relation-collection-displayTitle").val(title);
	$(container).find(".relation-collection-displayTitle").trigger('onchange');
	
	$(container).find(".card-title").html("<a target='_blank' href='" + entityId + "'>" + title + "</a>");
	$(container).find(".card-text").html("ID: " + entityId);
	
	if (select) {
		$(container).find(".relatedCollection-display").removeClass("hide");
		$(container).find(".relatedCollection-display-null").addClass("hide");
	} else {
		$(container).find(".relatedCollection-display").addClass("hide");
		$(container).find(".relatedCollection-display-null").removeClass("hide");
	}
};

CollectionEditor.prototype.handleRelationDirectionRadioChange = function(element) {
	var value;
	var bidirectional;
	
	if ($(element).val()=="right") {
		value = "<i class='fas fa-long-arrow-alt-right' aria-hidden='true'></i>";
		bidirectional = false;
	} else if ($(element).val()=="left") {
		value = "<i class='fas fa-long-arrow-alt-left' aria-hidden='true'></i>";
		bidirectional = false;
	} else {
		value = "<i class='fas fa-arrows-alt-h' aria-hidden='true'></i>";
		bidirectional = true;
	}
	editor.tables['relationTable'].handleInputChange(element, 'relationTable_direction', value, true);
	$(element).closest(".form-group").find(".relation-direction-bidirectional").val(bidirectional);
	
	var relatedCollectionContainer = $(element).closest("td").find(".relation-collection-container"); 	
	if ($(element).val()=="right" || $(element).val()=="bidirectional") {
		$(relatedCollectionContainer).find(".attribute-name-helper").text("relations{}.targetEntityId");
	} else {
		$(relatedCollectionContainer).find(".attribute-name-helper").text("relations{}.sourceEntityId");
	}
	editor.tables['relationTable'].sort();	
}
	
CollectionEditor.prototype.registerAgentRelationTypeSelection = function(element) {
	// Update displayed table list-row content based on AgentRelationType selection
	$(element).on("change", function() {
		var strSelected = "";
		$(this).find(":selected").each(function(i, selected) {
			strSelected += $(selected).text() + " ";
		});
		$(this).closest(".form-group").find(".agent-type-display-helper").val(strSelected).trigger('change');
	});
}

CollectionEditor.prototype.handleAccessTypeChange = function(select) {
	var editField = $(select).closest("td");
	
	if ($(select).find(":selected").text()==="OAI-PMH") {
		this.showHideParams(editField.find(".editor-list-accessparams"), ["set", "metadataPrefix"]);
		editField.find(".access-params").show();
		editField.find(".access-datamodels").show();
		editField.find(".patterns").hide();	
		editField.find(".test-access").hide();
		editField.find(".access-model-id").hide();
		editField.find(".subtype").hide();
	} else if ($(select).find(":selected").text()==="Git Repository") {
		this.showHideParams(editField.find(".editor-list-accessparams"), ["branch"]);
		editField.find(".access-params").show();
		editField.find(".access-datamodels").show();
		editField.find(".patterns").show();
		editField.find(".test-access").show();
		editField.find(".access-model-id").hide();
		editField.find(".subtype").show();
	} else if ($(select).find(":selected").text()==="REST API") {
		this.showHideParams(editField.find(".editor-list-accessparams"), false);
		editField.find(".access-params").show();
		editField.find(".access-datamodels").show();
		editField.find(".patterns").hide();
		editField.find(".test-access").hide();
		editField.find(".access-model-id").show();
		editField.find(".subtype").show();
	} else if ($(select).find(":selected").val()==="5808ae19c3c6002a42867d09") {
		this.showHideParams(editField.find(".editor-list-accessparams"), true);
		editField.find(".access-params").show();
		editField.find(".access-datamodels").show();
		editField.find(".patterns").hide();
		editField.find(".subtype").show();
		editField.find(".access-model-id").show();
	} else {
		this.showHideParams(editField.find(".editor-list-accessparams"), false);
		editField.find(".access-params").hide();
		editField.find(".access-datamodels").hide();
		editField.find(".patterns").hide();
		editField.find(".subtype").hide();
		editField.find(".access-model-id").hide();
	}
};

CollectionEditor.prototype.showHideParams = function(container, show) {	
	if (show==true) {
		return;
	}
	
	var existing = [];
	
	container.find(".editor-list-item").each(function() {
		var keep = false;
		var itemKey = $(this).find(".editor-list-input-param-key").val();
		if (show!=false && itemKey!="") {
			for (let key of show) {
				if ( itemKey==key ) {
					keep=true;
					existing.push(show[i]);
					break;
				}
			} 
		}
		if (!keep) {
			$(this).find(".btn-remove-entry").trigger("click");
		}	
	});	
	if (show!=false) {
		for (let key of show) {
			var found = false;
			for (let exKey of existing) {
				if (exKey==key) {
					found=true;
					break;
				}
			}
			if (!found) {
				container.find(".btn-add-list-element").trigger("click", key);
				container.find(".editor-list-item").last(function() {
					$(this).find(".editor-list-input-param-key").val(key);
				});
			}
		}
	}
}

CollectionEditor.prototype.handleAgentSelection = function(select, control, suggestion) {
	var _this = this;
	var formGroup = $(control).closest(".form-group"); 
	formGroup.find("input[type='hidden']").val(suggestion!=null ? suggestion.entityId : "");
	formGroup.find(".agent-name-display-helper").val(suggestion!=null ? (suggestion.name + " " + (suggestion.foreName!==undefined ? suggestion.foreName : "")) : "").trigger('change');
	
	if (select) {
		formGroup.find(".agent-display .card-title").html(
				"<a href='" + suggestion.entityId + "'>" +
					suggestion.name + " " + (suggestion.foreName!==undefined ? suggestion.foreName : "") +
				"</a>");
		formGroup.find(".agent-display .card-text").html("ID: " + suggestion.entityId);
		
		formGroup.find(".agent-display").removeClass("hide");
		formGroup.find(".agent-display-null").addClass("hide");
	} else {
		formGroup.find(".agent-display .card-title").text("");
		formGroup.find(".agent-display .card-text").text("");
		formGroup.find(".agent-display").addClass("hide");
		formGroup.find(".agent-display-null").removeClass("hide");
	}
};

CollectionEditor.prototype.renderCollectionSuggestion = function(collection) {
	var result = "<strong>" + collection.localizedDescriptions[0].title + "</strong><br />" +
			"<small><em>ID:" + collection.entityId + "</em></small>";
	
	if (collection.draftUser!=null && collection.draftUser!=undefined) {
		result += "<span class='label label-warning'>" + __translator.translate("~eu.dariah.de.colreg.common.labels.draft") + "</span>";
	}
	
	return result;
};

CollectionEditor.prototype.renderAgentSuggestion = function(agent) {
	return  "<strong>" + agent.name + " " + (agent.foreName!==undefined ? agent.foreName : "") + "</strong><br />" +
			"<small><em>ID:" + agent.entityId + "</em></small>";
};

CollectionEditor.prototype.triggerAddUnitOfMeasurement = function() {
	bootbox.prompt(__translator.translate("~eu.dariah.de.colreg.view.collection.labels.add_uom"), function(result){ 
		$.ajax({
	        url: __util.composeUrl("vocabularies/uom/async/add"),
	        data: { value: result },
	        type: "GET",
	        async: false,
	        encoding: "UTF-8",
	        dataType: "json",
	        success: function(data) {
	        	if (!data.success) {
	        		$("#uom-hint .uom-error-text").html("<small>" + data.message.messageBody + "</small>")
	        		$("#uom-hint").show();
	        	} else {
	        		var newoption = $("<option>").prop("value", data.pojo.id).text(data.pojo.name);
	        		$("#uomId").append(newoption);
	        		$("#uomId option[value=" + data.pojo.id + "]").attr("selected", "selected");
	        		$("#uom-hint").hide();
	        	}
	        }
	    });
	}); 
};

CollectionEditor.prototype.triggerTestAccess = function(control) {
	var _this = this;
	var form_identifier = this.collectionId + "-test-access";
	var container = $(control).closest("td"); 
	
	var accessSource = {
		type: $(container).find(".access-type select option:selected").val(),
		uri: $(container).find(".access-uri input").val(),
		set: $(container).find(".access-set input").val(),
		patterns: [],
		uuid: $("#uuid").val()
	};
	$(container).find(".access-patterns input").each(function() {
		accessSource.patterns.push($(this).val());
	});

	modalFormHandler = new ModalFormHandler({
		formFullUrl: __util.composeUrl("testAccess/form"),
		method: "POST",
		data: JSON.stringify(accessSource),
		contentType: "application/json; charset=utf-8",
		identifier: form_identifier,
		additionalModalClasses: "modal-lg",
		completeCallback: function(container) { 
			_this.refresh(); 
		},
		displayCallback: function() { 
			var testAccessHandler = new TestAccessHandler(accessSource);
			testAccessHandler.startTest();
		}
	});	
	modalFormHandler.show(form_identifier);
};