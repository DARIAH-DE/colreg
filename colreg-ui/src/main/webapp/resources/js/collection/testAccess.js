var TestAccessHandler = function(accessSource) {
	this.accessSource = accessSource;
};

TestAccessHandler.prototype.startTest = function() {
	var _this = this;
	this.executeTest("testAccess/tests/accessible", "~eu.dariah.de.colreg.view.access.test.accessibility",
		function() { 
			_this.executeTest("testAccess/tests/clone", "~eu.dariah.de.colreg.view.access.test.clone_or_pull",
				function() {
					_this.executeTest("testAccess/tests/patterns", "~eu.dariah.de.colreg.view.access.test.apply_pattern",
					function(data, container) {
						if (data.pojo==null || data.pojo.length==0) {
							container.append("<h3>" + __translator.translate("~eu.dariah.de.colreg.view.access.test.resulting_files", 0) +  " <i class='fas fa-check fa-color-warning'></i>");		
							container.append("<div class='alert alert-warning'>" + __translator.translate("~eu.dariah.de.colreg.view.access.test.errors.no_files") + "</div>");			
						} else {
							container.append("<h3>" + __translator.translate("~eu.dariah.de.colreg.view.access.test.resulting_files", data.pojo.length) +  " <i class='fas fa-check fa-color-success'></i>");
							if (data.pojo.length==1) {
								container.append("<div class='alert alert-success'>" + data.pojo[0] + "</div>");
							} else if (data.pojo.length>1) {
								var info = $("<ul style='max-height: 500px; overflow: scroll;' class='alert alert-success'>");
								for (var i=0; i<data.pojo.length; i++) {
									info.append("<li>" + data.pojo[i] + "</li");
									if (i>200) {
										info.append("<li>...</li");
										break;
									}
								}
								container.append(info);
							}
						}
					});
				}
			)
		}
	);
};

TestAccessHandler.prototype.executeTest = function(url, messageCode, successCallback) {
	var _this = this;
	var container = $("<div class='col-12'>");
	
	container.append("<h3>" + __translator.translate(messageCode) +  " <i class='fas fa-circle-notch fa-spin'></i></h3>");
	
	$("#access-test-messages").append(container);
	$.ajax({
        url: __util.composeUrl(url),
        data: JSON.stringify(this.accessSource),
        contentType: "application/json; charset=utf-8",
        type: "POST",
        dataType: "json",
        success: function(data, textStatus, jqXHR) {
        	_this.appendResult(data, container);
        	if (data.success==true && successCallback != undefined && typeof successCallback == 'function') {
        		successCallback(data, container);
        	}
        },
        error: function(jqXHR, textStatus, errorThrown) {}
	});
};

TestAccessHandler.prototype.appendResult = function(data, container) {
	
	container.find("h3 i").remove();
	
	if (data.success && data.objectWarnings.length==0) {
		container.find("h3").append(" <i class='fas fa-check fa-color-success'></i>");
	} else if (data.success) {
		container.find("h3").append(" <i class='fas fa-check fa-color-warning'></i>");
	} else {
		container.find("h3").append(" <i class='fas fa-exclamation fa-color-danger'></i> ");
	}
	
	if (data.objectErrors.length==1) {
		container.append("<div class='alert alert-danger'>" + data.objectErrors[0] + "</div>");
	} else if (data.objectErrors.length>1) {
		var errors = $("<ul class='alert alert-danger'>");
		for (var i=0; i<data.objectErrors.length; i++) {
			errors.append("<li>" + data.objectErrors[i] + "</li");
		}
		container.append(errors);
	}
	if (data.objectWarnings.length==1) {
		container.append("<div class='alert alert-warning'>" + data.objectWarnings[0] + "</div>");
	} else if (data.objectWarnings.length>1) {
		var warnings = $("<ul class='alert alert-warning'>");
		for (var i=0; i<data.objectWarnings.length; i++) {
			warnings.append("<li>" + data.objectWarnings[i] + "</li");
		}
		container.append(warnings);
	}	
};