var CollectionEditorList = function(options) {
	this.options = $.extend(true, 
			{
				// Should be overriden
				listSelector: ".editor-list",
				newRowUrl: "?",
				newRowCallback: null,
				
				// Defaults should be ok
				attributeNameHelperSelector: ".attribute-name-helper",
				listItemSelector: ".editor-list-item",
				listButtonSelector: ".editor-list-buttons",
				addButtonSelector: ".btn-collection-editor-add",
				pushUpButtonSelector: ".btn-push-up",
				pushDownButtonSelector: ".btn-push-down",
			}, 
			options);
	
	if (this.options.owner!==undefined) {
		this.options.owner.childElements.push(this);
	}
	
	this.sort();
}

CollectionEditorList.prototype.triggerAddListElement = function(btn, data) {
	var _this = this;
	$.ajax({
        url: _this.options.newRowUrl,
        type: "GET",
        data: {q : data},
        dataType: "html",
        success: function(data) {
        	var r = $(data);        	
        	_this.addNewEntry(r, $(btn).closest(_this.getSelector()));
        	if (_this.options.newRowCallback!==null && _this.options.newRowCallback!==undefined & 
        			typeof _this.options.newRowCallback==='function') {
        		_this.options.newRowCallback(r);
        	}
        },
        error: function(textStatus) { }
	});
};

CollectionEditorList.prototype.addNewEntry = function(item, list) {
	// Insert new rows before button row
	list.find(this.options.listButtonSelector).before(item);
	this.sort(list);
}

CollectionEditorList.prototype.removeEntry = function(btn) {
	var _this = this;
	$(btn).closest(this.options.listItemSelector).remove();
	this.sort($(btn).closest(_this.getSelector()));
};

CollectionEditorList.prototype.pushEntryUp = function(btn) {
	console.log("up");
	var _this = this;
	var prevEntry = $(btn).closest(this.options.listItemSelector).prev();
	
	if (prevEntry.length>0) {
		prevEntry.before($(btn).closest(this.options.listItemSelector).detach());
		this.sort($(btn).closest(_this.getSelector()));
	}
};

CollectionEditorList.prototype.pushEntryDown = function(btn) {
	var _this = this;
	var nextEntry = $(btn).closest(this.options.listItemSelector).next();
	
	if (nextEntry.length>0) {
		nextEntry.after($(btn).closest(this.options.listItemSelector).detach());		
		this.sort($(btn).closest(_this.getSelector()));
	}
};

CollectionEditorList.prototype.getSelector = function() {
	if (this.options.listSelector!==null && this.options.listSelector!==undefined & 
			typeof this.options.listSelector==='function') {
		return this.options.listSelector();
	} else {
		return this.options.listSelector;
	}
};

CollectionEditorList.prototype.sort = function(list, ownerTriggered) {
	var _this = this;
	
	if (list===undefined) {
		list = $(_this.getSelector());
	}
	
	if(ownerTriggered!==true && this.options.owner!==undefined) {
		this.options.owner.sort();
	}
	
	var index = 0;
	var lastItem = null;
	list.find(this.options.listItemSelector).each(function() {
		$(this).find(_this.options.attributeNameHelperSelector).each(function() {
			var element = $(this).next();
			if (!element.hasClass("form-control") && !element.hasClass("form-control-container")) {
				element = element.find(".form-control").not(".tt-hint").first();
			}
			
			if (element!==undefined && element.length>0) {
				var name;
				var id;
				if(_this.options.owner!==undefined) {
					// Use existing name and id as reset/prepared by owner
					name = $(element).prop("name").replace("{}", "[" + index + "]");
					id = $(element).prop("id").replace("{}", index);
				} else {
					// Use name from attributeNameHelperSelector
					name = $(this).text().replace("{}", "[" + index + "]");
					id = $(this).text().replace("{}", index);
				}
				element.prop("id", id).prop("name", name);
			}
			
		});
		
		$(this).find(_this.options.pushDownButtonSelector).addClass("disabled");
		if (index==0) {
			$(this).find(_this.options.pushUpButtonSelector).addClass("disabled");
		} else {
			$(this).find(_this.options.pushUpButtonSelector).removeClass("disabled");
		}
		if (lastItem!=null) {
			$(lastItem).find(_this.options.pushDownButtonSelector).removeClass("disabled");
		}
		lastItem = $(this);
		
		index++;
	});
}