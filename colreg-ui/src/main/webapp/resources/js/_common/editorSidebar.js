var editorSidebar = null;
$(document).ready(function() {
	editorSidebar = new EditorSidebar({});
});
$(window).resize(function() {
	editorSidebar.resize();
});

var EditorSidebar = function(data) {
	this.data = {
		sidebar: $("#editor-sidebar"),
		sidebarContainer: $("#editor-sidebar").parent(),
		sizeBlockSelector: "#editor-nav-sidebar"
	};
	$.extend(true, this.data, data);
	this.resize();
	
	this.data.sidebar.css("visibility", "visible");
};

EditorSidebar.prototype.resize = function() {
	this.showEditorLinkLists();
	this.showEditorLinkBlocks();
	if (this.getContainerHeight() > $(window).height()) {
		this.hideEditorLinkLists();
		if (this.getContainerHeight() > $(window).height()) {
			this.hideEditorLinkBlocks();	
		}
	}
};

EditorSidebar.prototype.getContainerHeight = function() {
	return this.data.sidebarContainer.height();
};

EditorSidebar.prototype.getSidebarHeight = function() {
	return this.data.sidebar.height();
};

EditorSidebar.prototype.hideEditorLinkLists = function() {
	this.data.sidebar.find(this.data.sizeBlockSelector + " ul").css("display", "none");
};

EditorSidebar.prototype.showEditorLinkLists = function() {
	this.data.sidebar.find(this.data.sizeBlockSelector + " ul").css("display", "");
};

EditorSidebar.prototype.hideEditorLinkBlocks = function() {
	this.data.sidebar.find(this.data.sizeBlockSelector).css("display", "none");
};

EditorSidebar.prototype.showEditorLinkBlocks = function() {
	this.data.sidebar.find(this.data.sizeBlockSelector).css("display", "");
};