# DARIAH-DE Collection Registry (CR)

The DARIAH-DE Collection Registry (CR) is a web application that serves as central component in the DARIAH-DE Data Federation Architecture (DFA) software stack. Its primary purpose consists in providing means to register, store and access descriptions of research data collections.

Further information on the concept of the Collection Registry can be accessed following https://de.dariah.eu/collection-registry.

## Installation

### Prerequisites

The Generic Search is a Java application currently has the following requirements:
* Java 11 (JRE)
* MongoDB

### APT package installation

#### 1. Add the repository key
````
wget -O - https://minfba.de.dariah.eu/minfba_public.asc | sudo apt-key add -
````

#### 2. Add the main and/or testing repository source and update

Main production packages:
````
echo "deb [arch=all] https://minfba.de.dariah.eu/nexus/repository/minfba-apt-releases/ any main" \
    | sudo tee /etc/apt/sources.list.d/minfba_repository.list
````

Testing packages:
````
echo "deb [arch=all] https://minfba.de.dariah.eu/nexus/repository/minfba-apt-testing/ any main" \
    | sudo tee /etc/apt/sources.list.d/minfba_repository.list
````

#### 3. Install the service

Update respository sources
````
sudo apt-get update
````

Install the package
````
sudo apt-get install dariah-colreg-ui
````

Install as service and enable
````
sudo systemctl start dariah-colreg-ui
sudo systemctl enable dariah-colreg-ui
````

For rare cases that require the installation of two parallel instances of the service, a alternative version is built and deployed as `dariah-colreg-ui-alt`, which can be installed without conflicting with the primary `dariah-colreg-ui` installation

### Java package installation

Release and snapshot packages can be downloaded from the Maven package repositories.

* For **release** packages see: https://minfba.de.dariah.eu/nexus/#browse/browse:minfba-releases:eu%2Fdariah%2Fde%2Fcolreg-ui
* For **snapshot** packages see: https://minfba.de.dariah.eu/nexus/#browse/browse:minfba-snapshots:eu%2Fdariah%2Fde%2Fcolreg-ui
