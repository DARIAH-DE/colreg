package eu.dariah.de.colreg.model;

import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class Access {
	@NotBlank
	private String type;
	private String subtype;
	
	@NotBlank(message="{~eu.dariah.de.colreg.validation.access.uri}")
	private String uri;
	private List<Datamodel> datamodels;
	private String description;
	private List<String> patterns;

	private String accessModelId;
	
	// GET or POST
	private String method;
	
	private List<AccessParam> headers;
	
	private List<AccessParam> params;
	
	
	public AccessParam getParam(String key) {
		if (params!=null) {
			for (AccessParam p : params) {
				if (p.getKey().equals(key)) {
					return p;
				}
			}
		}
		return null;
	}
	
	public void setParam(String key, String value) {
		if (params==null) {
			params = new ArrayList<>();
		}
		AccessParam p = new AccessParam();
		p.setKey(key);
		p.setValue(value);
		params.add(p);
	}
	
	/**
	 * This methods is mainly still there to keep Collections API backwards compatible 
	 * @deprecated
	 * @return List of datamodel IDs;
	 */
	public List<String> getSchemeIds() { 
		if (this.datamodels==null) {
			return new ArrayList<>(0);
		}
		List<String> schemeIds = new ArrayList<>(this.datamodels.size());
		for (Datamodel dm : this.datamodels) {
			if (dm.getModelId()!=null) {
				schemeIds.add(dm.getModelId());
			}
		}
		return schemeIds;
	}
	
}