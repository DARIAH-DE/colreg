package eu.dariah.de.colreg.model.vocabulary;

import java.util.List;
import java.util.Map;

import de.unibamberg.minf.dme.model.base.BaseIdentifiable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class LicenseGroup extends BaseIdentifiable {
	private static final long serialVersionUID = 3221594182077684205L;
	
	private List<License> licenses;
	private String identifier;
	private Map<String, String> labels;
}