package eu.dariah.de.colreg.model.vocabulary;

import de.unibamberg.minf.dme.model.base.BaseIdentifiable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class AccrualPeriodicity extends BaseIdentifiable {
	private static final long serialVersionUID = 185206577085697521L;
	
	private String identifier;
	private String label;
	private String description;
}
