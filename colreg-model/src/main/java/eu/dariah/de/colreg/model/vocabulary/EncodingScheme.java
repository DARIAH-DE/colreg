package eu.dariah.de.colreg.model.vocabulary;

import de.unibamberg.minf.dme.model.base.BaseIdentifiable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class EncodingScheme extends BaseIdentifiable {
	private static final long serialVersionUID = 8904831004241137209L;
	
	private String name;
	private String url;
}
