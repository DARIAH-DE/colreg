package eu.dariah.de.colreg.pojo.view;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import de.unibamberg.minf.dme.model.base.BaseIdentifiable;
import eu.dariah.de.colreg.pojo.ImagePojo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CollectionViewPojo extends BaseIdentifiable {
	private static final long serialVersionUID = 110389795212453807L;

	private boolean draft;
	private boolean published;
	private boolean deleted;
	
	private ImagePojo primaryImage;
	private String displayTitle;
	private List<String> collectionTypeIdentifiers;
	private List<String> accessTypes;
	
	private Long timestamp;
	private String displayTimestamp;
	
	
	public boolean isValid() {
		// Not possible through UI, but by deleting VocabularyItems
		return this.getCollectionTypeIdentifiers()!=null && !this.getCollectionTypeIdentifiers().isEmpty();
	}
}