package eu.dariah.de.colreg.pojo.api;

import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;

import eu.dariah.de.colreg.pojo.AccessPojo;
import eu.dariah.de.colreg.pojo.AccrualPojo;
import eu.dariah.de.colreg.pojo.ImagePojo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@XmlRootElement(name="collection")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExtendedCollectionApiPojo extends CollectionApiPojo {
	private static final long serialVersionUID = 5030563485613906700L;
	
	private String localizedDescription;
	private Map<String, String> decriptions;
	
	@XmlElementWrapper(name="access_methods")
	@XmlElement(name="access")
	private List<AccessPojo> accessPojos;
	
	@XmlElementWrapper(name="accrual_methods")
	@XmlElement(name="accrual")
	private List<AccrualPojo> accrualPojos;
	
	private String webPage;
	private String eMail;
	private boolean researchDriven;
	private boolean curationDriven;
	
	@XmlElementWrapper(name="images")
	@XmlElement(name="image")
	private List<ImagePojo> images;
	
	private Map<String, String> params;
}
