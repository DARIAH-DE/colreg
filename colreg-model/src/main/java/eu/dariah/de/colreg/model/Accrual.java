package eu.dariah.de.colreg.model;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class Accrual {
	@NotBlank private String accrualMethod;
	@NotBlank private String accrualPolicy;
	@NotBlank private String accrualPeriodicity;
	private String description;
}
