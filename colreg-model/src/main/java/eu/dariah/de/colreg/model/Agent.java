package eu.dariah.de.colreg.model;

import java.util.List;
import java.util.Map;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.URL;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import eu.dariah.de.colreg.model.base.VersionedEntityImpl;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class Agent extends VersionedEntityImpl {
	private static final long serialVersionUID = 2095721969639781396L;

	@NotBlank
	private String agentTypeId;
	
	@NotBlank(message="{~eu.dariah.de.colreg.validation.agent.name}")
	private String name;
	private String foreName;
	
	private List<Address> addresses;
	
	@Email(message="{~eu.dariah.de.colreg.validation.agent.email}")
	private String eMail;
	
	@URL(message="{~eu.dariah.de.colreg.validation.agent.webpage}")
	private String webPage;
	private String phone;
	
	private String gndId;
	private List<String> providedIdentifier;
	private String parentAgentId;
	private boolean deleted;
	private Map<Integer, String> agentImages;
	private String agentImageRights;
}