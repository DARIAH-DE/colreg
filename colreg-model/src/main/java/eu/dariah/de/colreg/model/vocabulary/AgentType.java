package eu.dariah.de.colreg.model.vocabulary;

import de.unibamberg.minf.dme.model.base.BaseIdentifiable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class AgentType extends BaseIdentifiable {
	private static final long serialVersionUID = -5716283876953029548L;
	
	private String identifier;
	private String label;
	private String description;
	private boolean naturalPerson;
}
