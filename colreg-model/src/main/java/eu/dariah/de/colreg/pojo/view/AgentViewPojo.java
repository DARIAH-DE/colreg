package eu.dariah.de.colreg.pojo.view;

import com.fasterxml.jackson.annotation.JsonInclude;

import de.unibamberg.minf.dme.model.base.BaseIdentifiable;
import eu.dariah.de.colreg.pojo.ImagePojo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AgentViewPojo extends BaseIdentifiable {
	private static final long serialVersionUID = -5544026876984535637L;

	private ImagePojo primaryImage;
	private boolean deleted;
	private Long timestamp;
	private String displayTimestamp;
	private String type;
	private String name;
}