package eu.dariah.de.colreg.pojo.view;

import java.util.ArrayList;
import java.util.List;

import de.unibamberg.minf.dme.model.base.Identifiable;
import lombok.Data;

@Data
public class TableListPojo<T extends Identifiable> {
	private List<TableObjectPojo<T>> aaData;
	
	public TableListPojo(List<T> data) {
		if (data==null) {
			this.aaData = null;
			return;
		}		
		this.aaData = new ArrayList<>(data.size());
		TableObjectPojo<T> wrappedEntity;
		for (T entity : data) {
			wrappedEntity = new TableObjectPojo<>(entity);
			this.aaData.add(wrappedEntity);
		}
	}
}
