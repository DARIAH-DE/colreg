package eu.dariah.de.colreg.pojo;

import java.io.Serializable;

import lombok.Data;

@Data
public class AccessParamPojo implements Serializable {
	private static final long serialVersionUID = 803725117349941145L;
	
	private String key;
	private String value;
}