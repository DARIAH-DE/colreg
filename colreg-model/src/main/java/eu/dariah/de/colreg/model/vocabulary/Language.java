package eu.dariah.de.colreg.model.vocabulary;

import de.unibamberg.minf.dme.model.base.BaseIdentifiable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class Language extends BaseIdentifiable {
	private static final long serialVersionUID = 8711972490509670083L;

	private String code;
	private String name;
}