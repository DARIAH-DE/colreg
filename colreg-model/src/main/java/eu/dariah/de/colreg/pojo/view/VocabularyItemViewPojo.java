package eu.dariah.de.colreg.pojo.view;

import com.fasterxml.jackson.annotation.JsonInclude;

import de.unibamberg.minf.dme.model.base.BaseIdentifiable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VocabularyItemViewPojo extends BaseIdentifiable implements Comparable<VocabularyItemViewPojo> {
	private static final long serialVersionUID = -3342978755459281019L;
	
	private String identifier;
	private String externalIdentifier;
	private String localizedLabel;
	private boolean hasCurrentLocale;
	private String defaultName;
	private String vocabularyId;
	private boolean deleted;
		
	public String getDisplayLabel() {
		if (this.hasCurrentLocale) {
			return this.localizedLabel;
		} else {
			return this.defaultName;
		}
	}
	
	@Override
	public int compareTo(VocabularyItemViewPojo o) {
		return this.getDisplayLabel().compareTo(o.getDisplayLabel());
	}
}