package eu.dariah.de.colreg.model;

import lombok.Data;

@Data
public class Datamodel {
	private String modelId;
	private String alias;
}