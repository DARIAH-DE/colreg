package eu.dariah.de.colreg.pojo.view;

import lombok.Data;

@Data
public class NodePojo {
	private String id;
	private String type;
	private String label;
}
