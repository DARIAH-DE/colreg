package eu.dariah.de.colreg.model;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
public class LocalizedDescription {
	@NotBlank(message="{~eu.dariah.de.colreg.validation.localized_description.language}")
	private String languageId;
	
	@NotBlank(message="{~eu.dariah.de.colreg.validation.localized_description.title}") 
	private String title;
	private String description;
	private String acronym;
		
	@JsonIgnore
	public boolean isEmpty() {
		return (languageId==null || languageId.trim().isEmpty()) &&
				(title==null || title.trim().isEmpty()) &&
				(description==null || description.trim().isEmpty()) &&
				(acronym==null || acronym.trim().isEmpty());
	}
}