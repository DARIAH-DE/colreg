package eu.dariah.de.colreg.model.vocabulary;

import de.unibamberg.minf.dme.model.base.BaseIdentifiable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class UnitOfMeasurement extends BaseIdentifiable {
	private static final long serialVersionUID = 8711972490509670083L;

	private String messageCode;
	private String name;
}