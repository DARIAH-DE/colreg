package eu.dariah.de.colreg.pojo.api;

import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;

import eu.dariah.de.colreg.pojo.ImagePojo;
import eu.dariah.de.colreg.pojo.base.BaseApiPojo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@XmlRootElement(name="collection")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CollectionApiPojo extends BaseApiPojo {
	private static final long serialVersionUID = -5544026876984535637L;

	private String versionId;
	private Long timestamp;
	private String parentId;
	
	private boolean draft;
	private boolean published;
	private boolean deleted;
	
	private List<String> identifiers;
	
	private ImagePojo primaryImage;

	/* If locale for conversion is provided */
	private String localizedTitle;
	private String localizedAcronym;
	private String localizedTimestamp;
	
	/* If no locale for conversion is provided */
	private Map<String, String> titles;
	private Map<String, String> acronyms;
	
	@XmlElementWrapper(name="agents")
	@XmlElement(name="agent")
	private List<AgentApiPojo> agents;
	
	@XmlElementWrapper(name="collection_types")
	@XmlElement(name="type")
	private List<String> collectionTypes;

	private List<String> itemLanguages;
	
	private List<String> subjects;
	private List<String> spatials;
	private List<String> temporals;
	
	private List<String> itemTypes;
}