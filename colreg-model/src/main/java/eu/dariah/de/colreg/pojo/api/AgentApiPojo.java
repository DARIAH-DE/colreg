package eu.dariah.de.colreg.pojo.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;

import eu.dariah.de.colreg.pojo.ImagePojo;
import eu.dariah.de.colreg.pojo.base.BaseApiPojo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@XmlRootElement(name="agent")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AgentApiPojo extends BaseApiPojo {
	private static final long serialVersionUID = -5544026876984535637L;

	private String versionId;
	private Long timestamp;
	private String parentId;
	private boolean deleted;
	private String type;
	private String name;
	private String lastChanged;
	private ImagePojo primaryImage;
	private String localizedTimestamp;
}
