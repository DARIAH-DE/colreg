package eu.dariah.de.colreg.pojo.view;

import java.util.List;

import lombok.Data;

@Data
public class GraphPojo {
	private List<EdgePojo> edges;
	private List<NodePojo> nodes;
}
