package eu.dariah.de.colreg.model.vocabulary;

import de.unibamberg.minf.dme.model.base.BaseIdentifiable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class AccessType extends BaseIdentifiable {
	private static final long serialVersionUID = -237738480385781253L;
	
	private String identifier;
	private boolean machineAccessible;
	private String label;
	private String description;
	private boolean canUsePatterns;
}
