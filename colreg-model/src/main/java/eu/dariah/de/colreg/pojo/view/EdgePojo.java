package eu.dariah.de.colreg.pojo.view;

import lombok.Data;

@Data
public class EdgePojo {
	private String source;
	private String target;
}
