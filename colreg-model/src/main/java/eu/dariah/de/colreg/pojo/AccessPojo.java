package eu.dariah.de.colreg.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@XmlRootElement(name="access")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccessPojo implements Serializable {
	private static final long serialVersionUID = 1594290319162434640L;
	
	private String type;
	private String subtype;
	private String method;
	private String uri;
	private String accessModelId;
	private List<DatamodelPojo> datamodels;
	private List<String> patterns;
	private List<AccessParamPojo> params;
	private List<AccessParamPojo> headers;
	
	private String set;
	private String prefix;
		
	/**
	 * API compatibility
	 * 
	 * @deprecated in favor of datamodels
	 * @return List of datamodel IDs;
	 */
	@Deprecated
	@XmlTransient
	public List<String> getSchemeIds() { 
		if (this.datamodels==null) {
			return new ArrayList<>(0);
		}
		List<String> result = new ArrayList<>();
		for (DatamodelPojo dm : this.datamodels) {
			result.add(dm.getId());
		}
		return result;
	}
	
	
	public AccessParamPojo getParam(String key) {
		if (params!=null) {
			for (AccessParamPojo p : params) {
				if (p.getKey().equals(key)) {
					return p;
				}
			}
		}
		return null;
	}
	
	/**
	 * This methods is mainly still there to keep Collections API backwards compatible 
	 * @deprecated
	 * @return String oaiSet;
	 */
	public String getOaiSet() {
		return this.getParam("set")!=null ? this.getParam("set").getValue() : "";
	}

	/**
	 * This methods is mainly still there to keep Collections API backwards compatible 
	 * @deprecated
	 * @return String metadataPrefix;
	 */
	public String getMetadataPrefix() {
		return this.getParam("metadataPrefix")!=null ? this.getParam("metadataPrefix").getValue() : "";
	}
}