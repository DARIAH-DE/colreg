package eu.dariah.de.colreg.pojo.api;

import java.util.List;

import lombok.Data;

@Data
public class TestAccessSourcePojo {
	private String type;
	private String uri;
	private String set;
	private List<String> patterns;
	private String uuid;
}
