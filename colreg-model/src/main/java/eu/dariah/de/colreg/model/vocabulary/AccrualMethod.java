package eu.dariah.de.colreg.model.vocabulary;

import de.unibamberg.minf.dme.model.base.BaseIdentifiable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class AccrualMethod extends BaseIdentifiable {
	private static final long serialVersionUID = -2581962798973602812L;
	
	private String identifier;
	private String label;
	private String description;
}
