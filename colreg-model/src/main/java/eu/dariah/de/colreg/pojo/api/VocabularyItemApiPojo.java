package eu.dariah.de.colreg.pojo.api;

import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;

import eu.dariah.de.colreg.pojo.base.BaseApiPojo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@XmlRootElement(name="vocabulary_item")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VocabularyItemApiPojo extends BaseApiPojo {
	private static final long serialVersionUID = -7649200109797919221L;
	private String identifier;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String externalIdentifier;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String localizedLabel;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Map<String, String> labels;
	
	private String defaultName;
	private String vocabularyId;
	private boolean deleted;
}