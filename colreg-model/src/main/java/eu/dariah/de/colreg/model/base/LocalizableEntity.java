package eu.dariah.de.colreg.model.base;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import de.unibamberg.minf.dme.model.base.BaseIdentifiable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public abstract class LocalizableEntity extends BaseIdentifiable {
	private static final long serialVersionUID = -1812502294817149433L;

	@NotBlank(message="{~eu.dariah.de.colreg.validation.vocabulary_item.identifier_blank}")
	@Pattern(regexp="([\\p{L}\\p{N}-_.])*",message="{~eu.dariah.de.colreg.validation.vocabulary_item.identifier_pattern}")
	private String identifier;
	
	@NotBlank(message="{~eu.dariah.de.colreg.validation.vocabulary_item.default_name_blank}")
	private String defaultName;
	
	private String externalIdentifier;
	private String description;
		
	public String getMessageCode() {
		if (this.identifier==null || this.identifier.trim().isEmpty()) {
			return null;
		}
		return this.getMessageCodePrefix() + this.identifier;
	}
	
	public abstract String getMessageCodePrefix();
}
