package eu.dariah.de.colreg.model;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class AccessParam {
	@NotBlank
	private String key;
	private String value;
}