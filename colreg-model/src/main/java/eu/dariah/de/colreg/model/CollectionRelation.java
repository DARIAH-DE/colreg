package eu.dariah.de.colreg.model;

import javax.validation.constraints.NotEmpty;

import de.unibamberg.minf.dme.model.base.BaseIdentifiable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class CollectionRelation extends BaseIdentifiable {
	private static final long serialVersionUID = 1157156390776127985L;

	public static final String COLLECTION_RELATION_TYPES_VOCABULARY_IDENTIFIER = "collectionRelationTypes";
	
	@NotEmpty(message="{~eu.dariah.de.colreg.validation.collection_relation.related_collection}")
	private String sourceEntityId;
	
	@NotEmpty(message="{~eu.dariah.de.colreg.validation.collection_relation.related_collection}")
	private String targetEntityId;
	private boolean bidirectional;
	private String relationTypeId;
	private String description;
	
	
	public boolean isSame(CollectionRelation relation) {
		return this.getRelationTypeId().equals(relation.getRelationTypeId()) &&
				this.areStringsSame(this.getSourceEntityId(), relation.getSourceEntityId()) && 
				this.areStringsSame(this.getTargetEntityId(), relation.getTargetEntityId()) &&
				this.areStringsSame(this.getDescription(), relation.getDescription()) &&
				this.isBidirectional()==relation.isBidirectional();
	}
}