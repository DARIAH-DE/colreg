package eu.dariah.de.colreg.model.base;

import org.joda.time.DateTime;
import org.springframework.data.mongodb.core.index.Indexed;

import de.unibamberg.minf.dme.model.base.BaseIdentifiable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public abstract class VersionedEntityImpl extends BaseIdentifiable {
	private static final long serialVersionUID = 7582379861649995596L;
	
	@Indexed 
	private String entityId;				// Persistent identifier of the entity (id is for the version) 
	private String succeedingVersionId;		// The versionId is actually the id of an object 
	private DateTime entityTimestamp;	
	private String entityCreator;
	private DateTime versionTimestamp;	
	private String versionCreator;
	private String versionComment;
}