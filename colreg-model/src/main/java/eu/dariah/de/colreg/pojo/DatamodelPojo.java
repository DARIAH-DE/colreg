package eu.dariah.de.colreg.pojo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@XmlRootElement(name="datamodel")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DatamodelPojo implements Serializable {
	private static final long serialVersionUID = -4392228874815102548L;
	
	private String id;
	private String alias;
}
