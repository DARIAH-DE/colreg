package eu.dariah.de.colreg.model;

import lombok.Data;

@Data
public class Address {
	private String street;
	private String number;
	private String postalCode;
	private String place;
	private String country;
	private String note;
	
	public boolean isEmpty() {
		return ((this.getStreet()==null || this.getStreet().trim().isEmpty()) && 
				(this.getNumber()==null || this.getNumber().trim().isEmpty()) && 
				(this.getPostalCode()==null || this.getPostalCode().trim().isEmpty()) && 
				(this.getPlace()==null || this.getPlace().trim().isEmpty()) && 
				(this.getCountry()==null || this.getCountry().trim().isEmpty()) && 
				(this.getNote()==null || this.getNote().trim().isEmpty()) );
	}
}