package eu.dariah.de.colreg.model.vocabulary;

import de.unibamberg.minf.dme.model.base.BaseIdentifiable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class CollectionType extends BaseIdentifiable {
	private static final long serialVersionUID = 5254354255079831197L;
	
	private String label;
	private String description;
}
