package eu.dariah.de.colreg.model.vocabulary;

import de.unibamberg.minf.dme.model.base.BaseIdentifiable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class CollectionAgentRelationType extends BaseIdentifiable {
	private static final long serialVersionUID = -5636572505149778721L;
	
	private String label;
	private String description;
}
