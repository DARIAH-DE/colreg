package eu.dariah.de.colreg.pojo.view;

import de.unibamberg.minf.dme.model.base.BaseIdentifiable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class CollectionRelationViewPojo extends BaseIdentifiable {
	private static final long serialVersionUID = -596585175519103225L;
	
	private CollectionViewPojo source;
	private CollectionViewPojo target;
	private boolean bidirectional;
	private String relationTypeId;
	private String description;
}