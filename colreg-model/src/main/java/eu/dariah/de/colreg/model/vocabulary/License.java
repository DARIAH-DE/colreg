package eu.dariah.de.colreg.model.vocabulary;

import java.beans.Transient;

import de.unibamberg.minf.dme.model.base.BaseIdentifiable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class License extends BaseIdentifiable {
	private static final long serialVersionUID = 8705409920105900752L;

	private LicenseGroup group;
	private String identifier;
	private String label;
	private String url;
	
	
	@Transient
	public LicenseGroup getGroup() { return group; }
}