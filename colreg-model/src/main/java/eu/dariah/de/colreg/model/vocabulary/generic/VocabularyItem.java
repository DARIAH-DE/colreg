package eu.dariah.de.colreg.model.vocabulary.generic;

import eu.dariah.de.colreg.model.base.LocalizableEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class VocabularyItem extends LocalizableEntity {
	private static final long serialVersionUID = 942372659078068387L;
		
	private String vocabularyIdentifier;
	private boolean deleted;
	
	@Override
	public String getMessageCodePrefix() {
		return Vocabulary.MESSAGE_CODE_PREFIX + this.getVocabularyIdentifier() + ".";
	}
}