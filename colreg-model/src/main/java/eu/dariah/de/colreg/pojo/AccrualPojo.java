package eu.dariah.de.colreg.pojo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@XmlRootElement(name="accrual")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccrualPojo implements Serializable {
	private static final long serialVersionUID = -3372813034204379952L;
	
	private String accrualMethod;
	private String accrualPolicy;
	private String accrualPeriodicity;
}
